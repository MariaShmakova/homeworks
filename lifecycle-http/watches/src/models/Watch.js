import { nanoid } from "nanoid";

class Watch {
  constructor({ title, timeZone }) {
    this.id = nanoid();
    this.title = title;
    this.timeZone = +timeZone;
  }
}

export default Watch;
