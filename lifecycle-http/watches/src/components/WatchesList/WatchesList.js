import React from "react";
import WatchesItem from "../WatchesItem/WatchesItem";
import "./WatchesList.scss";

function WatchesList({ list, onRemove }) {
  return (
    <div className="WatchesList">
      {list.map((item) => (
        <WatchesItem
          key={item.id}
          title={item.title}
          timeZone={item.timeZone}
          onRemove={() => onRemove(item.id)}
        />
      ))}
    </div>
  );
}

export default WatchesList;
