import React from "react";
import "./WatchesItemDigital.scss";
import PropTypes from "prop-types";

WatchesItemDigital.propTypes = {
  time: PropTypes.shape({
    h: PropTypes.number.isRequired,
    m: PropTypes.number.isRequired,
    s: PropTypes.number.isRequired,
  }),
};

function WatchesItemDigital({ time }) {
  const { h, m, s } = time;
  return (
    <div className="WatchesItemDigital">
      {h}:{m}:{s}
    </div>
  );
}

export default WatchesItemDigital;
