import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import WatchesItemAnalog from "./WatchesItemAnalog";
import WatchesItemDigital from "./WatchesItemDigital";
import "./WatchesItem.scss";

WatchesItem.propTypes = {
  title: PropTypes.string.isRequired,
  timeZone: PropTypes.number.isRequired,
};

/**
 * Компонент часов
 *
 * @param title - название
 * @param timeZone - временная зона
 * @param onRemove - метод для удаления часов
 * @returns {JSX.Element}
 * @constructor
 */
function WatchesItem({ title, timeZone, onRemove }) {
  const [time, setTime] = useState({ h: 0, m: 0, s: 0 });

  const getCurrentTime = () => {
    const currentTime = new Date();
    const utc = currentTime.getTime() + currentTime.getTimezoneOffset() * 60000;
    const date = new Date(utc + 3600000 * timeZone);
    setTime({ h: date.getHours(), m: date.getMinutes(), s: date.getSeconds() });
  };

  useEffect(() => {
    getCurrentTime();

    const id = setInterval(getCurrentTime, 500);
    return () => {
      clearInterval(id);
    };
  }, []);

  return (
    <div className="WatchesItem">
      <button
        type="button"
        className="btn-close"
        aria-label="Close"
        onClick={onRemove}
      />
      <h2 className="WatchesItem-title">{title}</h2>
      <div className="WatchesItem-clock">
        <WatchesItemAnalog time={time} />
        <WatchesItemDigital time={time} />
      </div>
    </div>
  );
}

export default WatchesItem;
