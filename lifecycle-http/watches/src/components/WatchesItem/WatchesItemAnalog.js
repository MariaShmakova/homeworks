import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import "./WatchesItemAnalog.scss";

WatchesItemAnalog.propTypes = {
  time: PropTypes.shape({
    h: PropTypes.number.isRequired,
    m: PropTypes.number.isRequired,
    s: PropTypes.number.isRequired,
  }),
};

function WatchesItemAnalog({ time }) {
  const svgEl = useRef(null);

  useEffect(() => {
    svgEl.current.style.setProperty("--start-seconds", time.s);
    svgEl.current.style.setProperty("--start-minutes", time.m);
    svgEl.current.style.setProperty("--start-hours", time.h % 12);
  }, [time]);

  return (
    <svg viewBox="0 0 40 40" ref={svgEl} className="WatchesItemAnalog">
      <circle cx="20" cy="20" r="19" />
      <Marks />
      <line x1="0" y1="0" x2="9" y2="0" className="hour" />
      <line x1="0" y1="0" x2="13" y2="0" className="minute" />
      <line x1="0" y1="0" x2="16" y2="0" className="seconds" />
      <circle cx="20" cy="20" r="0.7" className="pin" />
    </svg>
  );
}

function Marks() {
  const marks = [];
  for (let i = 0; i < 12; i++) {
    marks.push(<line key={i} x1="15" y1="0" x2="16" y2="0" />);
  }
  return <g className="marks">{marks}</g>;
}

export default WatchesItemAnalog;
