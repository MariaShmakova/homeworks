import React, { useState } from "react";
import AddWatchesForm from "../AddWatchesForm/AddWatchesForm";
import WatchesList from "../WatchesList/WatchesList";
import Watch from "../../models/Watch";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  const [watches, setWatches] = useState([]);

  const handleSubmit = (data) => {
    setWatches((prevState) => [...prevState, new Watch(data)]);
  };
  const handleRemove = (id) => {
    setWatches((prevState) => prevState.filter((item) => item.id !== id));
  };

  return (
    <div className="App container">
      <div className="row">
        <div className="col-md-6">
          <AddWatchesForm onSubmit={handleSubmit} />
        </div>
        <div className="col-md-6">
          <WatchesList list={watches} onRemove={handleRemove} />
        </div>
      </div>
    </div>
  );
}

export default App;
