import React, { useState } from "react";
import PropTypes from "prop-types";

const INITIAL_FORM = {
  title: "",
  timeZone: "",
};

/**
 * Форма добавления часов
 *
 * @param onSubmit - метод для сохранения добавленных часов
 * @returns {JSX.Element}
 * @constructor
 */
function AddWatchesForm({ onSubmit }) {
  const [form, setForm] = useState(INITIAL_FORM);

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(form);
    setForm(INITIAL_FORM);
  };

  const handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    setForm((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <form className="AddWatchesForm" onSubmit={handleSubmit}>
      <div className="mb-3">
        <label htmlFor="title" className="form-label">
          Название
        </label>
        <input
          value={form.title}
          name="title"
          type="text"
          className="form-control"
          onChange={handleChange}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="timeZone" className="form-label">
          Временная зона
        </label>
        <input
          value={form.timeZone}
          name="timeZone"
          type="number"
          className="form-control"
          onChange={handleChange}
        />
      </div>
      <button className="btn btn-primary">Добавить</button>
    </form>
  );
}

AddWatchesForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default AddWatchesForm;
