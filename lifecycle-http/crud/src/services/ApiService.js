import axios from "axios";

const CancelToken = axios.CancelToken;
let getNotesSource;

export default class ApiService {
  api = axios.create({
    baseURL: process.env.REACT_APP_URL,
  });

  addNotes = async (data) => {
    try {
      const res = await this.api.post("/notes", data);
      return res.status === 200;
    } catch (e) {
      console.error(e);
    }
  };

  delNotes = async (id) => {
    try {
      const res = await this.api.delete(`/notes/${id}`);
      return res.status === 204;
    } catch (e) {
      console.error(e);
    }
  };

  getNotes = async () => {
    try {
      getNotesSource && getNotesSource.cancel();
      getNotesSource = CancelToken.source();

      const res = await this.api.get("/notes", {
        cancelToken: getNotesSource.token,
      });
      return res.data;
    } catch (e) {
      if (axios.isCancel(e)) {
        return false;
      } else {
        console.error(e);
      }
    }
  };
}
