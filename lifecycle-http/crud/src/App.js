import React, { useState, useEffect } from "react";
import NotesList from "./components/Notes/NotesList";
import NotesAdd from "./components/Notes/NotesAdd";

import "bootstrap/dist/css/bootstrap.min.css";
import ApiService from "./services/ApiService";
import Loader from "./components/Loader/Loader";

const api = new ApiService();
const INITIAL_LOADING = {
  get: false,
  add: false,
  del: false,
};

function App() {
  const [notes, setNotes] = useState([]);
  const [loading, setLoading] = useState(INITIAL_LOADING);

  useEffect(() => {
    getNotes();
  }, []);

  const getNotes = async () => {
    setLoading((prevState) => ({ ...prevState, get: true }));
    const res = await api.getNotes();
    res && setNotes(res);
    setLoading((prevState) => ({ ...prevState, get: false }));
  };

  const handleDelete = async (id) => {
    setLoading((prevState) => ({ ...prevState, del: id }));
    await api.delNotes(id);
    setLoading((prevState) => ({ ...prevState, del: false }));
    await getNotes();
  };

  const handleAdd = async (text) => {
    setLoading((prevState) => ({ ...prevState, add: true }));
    await api.addNotes({
      content: text,
    });
    setLoading((prevState) => ({ ...prevState, add: false }));
    await getNotes();
  };

  return (
    <div className="App container mt-4">
      <button className="btn btn-success mb-3" onClick={getNotes}>
        {loading.get && <Loader />}
        Update Notes
      </button>
      <div className="row">
        <div className="col">
          <NotesAdd onSubmit={handleAdd} isLoading={loading.add} />
        </div>
        <div className="col">
          <NotesList
            list={notes}
            onDelete={handleDelete}
            isLoading={loading.del}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
