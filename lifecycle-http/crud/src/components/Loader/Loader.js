import React from "react";

function Loader() {
  return (
    <span
      className="Loader spinner-border spinner-border-sm me-2"
      role="status"
    >
      <span className="visually-hidden">Loading...</span>
    </span>
  );
}

export default Loader;
