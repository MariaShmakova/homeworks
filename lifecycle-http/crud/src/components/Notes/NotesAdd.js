import React, { useState } from "react";
import Loader from "../Loader/Loader";

function NotesAdd({ isLoading, onSubmit }) {
  const [text, setText] = useState("");
  const [error, setError] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!text || !text.length) {
      setError("Поле обязательно для заполнения");
      return;
    }
    onSubmit(text);
    setText("");
  };
  const handleChangeText = (e) => {
    setError(null);
    setText(e.target.value);
  };

  const Error = () => {
    return <div className="invalid-feedback">{error}</div>;
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-3">
        <label htmlFor="text" className="form-label">
          New note
        </label>
        <textarea
          className={error ? "form-control is-invalid" : "form-control"}
          id="text"
          rows="3"
          value={text}
          onChange={handleChangeText}
        />{" "}
        {error && <Error />}
      </div>
      <button type="submit" className="btn btn-primary" disabled={isLoading}>
        {isLoading && <Loader />}
        Add
      </button>
    </form>
  );
}

export default NotesAdd;
