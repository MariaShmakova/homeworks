import React from "react";
import NotesItem from "./NotesItem";

function NotesList({ list, onDelete, isLoading }) {
  return (
    <div className="NotesList">
      {list.map(({ id, content }) => (
        <NotesItem
          key={id}
          content={content}
          onDelete={() => onDelete(id)}
          isLoading={isLoading === id}
        />
      ))}
    </div>
  );
}

export default NotesList;
