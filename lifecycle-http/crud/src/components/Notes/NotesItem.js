import React from "react";
import Loader from "../Loader/Loader";

function NotesItem({ content, isLoading, onDelete }) {
  return (
    <div className="NotesItem card mb-3" style={{ width: "18rem" }}>
      <div className="card-body">
        <p className="card-text">{content}</p>
        <button
          className="btn btn-primary"
          onClick={onDelete}
          disabled={isLoading}
        >
          {isLoading && <Loader />}
          del
        </button>
      </div>
    </div>
  );
}

export default NotesItem;
