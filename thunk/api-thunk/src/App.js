import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import Services from "./page/Services";
import ServiceItem from "./page/ServiceItem";
import Notices from "./components/Notices";

function App() {
  return (
    <Router>
      <div className="container p-4">
        <Switch>
          <Route path="/services/:id" component={ServiceItem} />
          <Route path="/services" component={Services} />
          <Redirect from="/" to="/services" />
        </Switch>
        <Notices />
      </div>
    </Router>
  );
}

export default App;
