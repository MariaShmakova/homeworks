import { addErrorNotice } from "../modules/notices";
import { noticeError } from "../modules/notices";

const notice = ({ dispatch }) => (next) => (action) => {
  if (action.type === noticeError.type) {
    dispatch(addErrorNotice(action.payload));
  } else return next(action);
};

export default notice;
