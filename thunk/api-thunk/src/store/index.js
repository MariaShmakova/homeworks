import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import reducer from "./modules";
import middleware from "./middleware";

const store = configureStore({
  reducer,
  middleware: [...getDefaultMiddleware(), ...middleware],
});

export default store;
