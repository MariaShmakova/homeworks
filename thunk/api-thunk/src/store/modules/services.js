import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { createSelector } from "reselect";
import api from "../../services/ApiService";
import { noticeError } from "./notices";

const fetchServices = createAsyncThunk(
  "services/fetch",
  async () => await api.getServices()
);

const removeService = createAsyncThunk(
  "services/remove",
  async (id, thunkAPI) => {
    try {
      return await api.removeServices(id);
    } catch (error) {
      thunkAPI.dispatch(
        noticeError("Произошла ошибка при удалении сервиса! Повторите запрос.")
      );
      throw error;
    }
  }
);

const slice = createSlice({
  name: "services",
  initialState: {
    list: [],
    loading: false,
    error: null,
    filter: "",
    removing_list: [],
  },
  reducers: {
    changeFilter: (services, action) => {
      services.filter = action.payload;
    },
  },
  extraReducers: {
    [fetchServices.pending]: (services, action) => {
      services.loading = true;
    },
    [fetchServices.fulfilled]: (services, action) => {
      services.list = action.payload;
      services.loading = false;
      services.error = null;
    },
    [fetchServices.rejected]: (services, action) => {
      services.error = action.error.message;
      services.loading = false;
    },
    [removeService.pending]: (services, action) => {
      services.removing_list.push(action.meta.arg);
    },
    [removeService.fulfilled]: (services, action) => {
      const id = action.meta.arg;
      services.list = services.list.filter((item) => item.id !== id);
      services.removing_list = services.removing_list.filter(
        (item) => item !== id
      );
    },
    [removeService.rejected]: (services, action) => {
      services.removing_list = services.removing_list.filter(
        (item) => item !== action.meta.arg
      );
    },
  },
});

const { changeFilter } = slice.actions;

const getServicesData = (state) => state.services;

const getFilteredServices = createSelector(
  (state) => state.services.list,
  (state) => state.services.filter,
  (services, filter) => {
    if (!filter || !filter.length) {
      return services;
    }
    return services.filter(
      (item) => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );
  }
);

export {
  fetchServices,
  removeService,
  changeFilter,
  getServicesData,
  getFilteredServices,
};
export default slice.reducer;
