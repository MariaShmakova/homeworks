import { combineReducers } from "redux";
import servicesReducer from "./services";
import serviceFormReducer from "./serviceForm";
import noticesReducer from "./notices";

export default combineReducers({
  services: servicesReducer,
  serviceForm: serviceFormReducer,
  notices: noticesReducer,
});
