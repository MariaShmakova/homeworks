import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchServices } from "./services";
import api from "../../services/ApiService";

const saveService = createAsyncThunk("service/save", async (data, thunkAPI) => {
  const response = await api.saveService(data);

  if (data.id === 0) {
    thunkAPI.dispatch(fetchServices());
    thunkAPI.dispatch(serviceClear());
  } else {
    // TODO: Help Не понимаю как реализовать через history
    window.location.href = "/";
  }

  return response;
});

const fetchService = createAsyncThunk(
  "service/fetch",
  async (id) => await api.getService(id)
);

const INITIAL_STATE = {
  form: {
    id: 0,
    name: "",
    price: "",
    content: "",
  },
  loading: false,
  error: null,
};

const slice = createSlice({
  name: "serviceForm",
  initialState: INITIAL_STATE,
  reducers: {
    serviceFormFieldChange: (state, action) => {
      const { name, value } = action.payload;
      state.form[name] = value;
    },

    serviceClear: (state, action) => {
      state.error = null;
      state.form = INITIAL_STATE.form;
    },
  },
  extraReducers: {
    [saveService.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [saveService.fulfilled]: (state) => {
      state.loading = false;
    },
    [saveService.rejected]: (state, action) => {
      state.error = action.error && action.error.message;
      state.loading = false;
    },
    [fetchService.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [fetchService.fulfilled]: (state, action) => {
      state.form = action.payload;
      state.loading = false;
    },
    [fetchService.rejected]: (state, action) => {
      state.error = action.error.message;
      state.loading = false;
    },
  },
});

const { serviceFormFieldChange, serviceClear } = slice.actions;

export { saveService, fetchService, serviceFormFieldChange, serviceClear };

export default slice.reducer;
