import { nanoid } from "nanoid";
import { createSlice, createAction } from "@reduxjs/toolkit";

const noticeError = createAction("notice/error");

const slice = createSlice({
  name: "notices",
  initialState: {
    list: [],
  },
  reducers: {
    addNotice: (notice, action) => {
      const { message, type } = action.payload;
      notice.list.push({
        id: nanoid(),
        message,
        type,
        created_at: new Date(),
      });
    },
    removeNotice: (notice, action) => {
      notice.list = notice.list.filter((item) => item.id !== action.payload);
    },
  },
});

const addErrorNotice = (error) => ({
  type: addNotice.type,
  payload: {
    type: "error",
    message: error,
  },
});

const { addNotice, removeNotice } = slice.actions;

export { noticeError, addErrorNotice, addNotice, removeNotice };

export default slice.reducer;
