const baseServiceUrl = `${process.env.REACT_APP_URL}/services`;
const api = {
  getServices: async () => {
    try {
      const response = await fetch(baseServiceUrl);

      if (!response.ok) {
        throw new Error(response.statusText);
      }

      return response.json();
    } catch (error) {
      throw new Error(error.message);
    }
  },

  removeServices: async (id) => {
    try {
      const response = await fetch(`${baseServiceUrl}/${id}`, {
        method: "DELETE",
      });
      if (!response.ok) {
        throw new Error(response.statusText);
      }

      return true;
    } catch (error) {
      throw new Error(error.message);
    }
  },

  saveService: async (data) => {
    try {
      const response = await fetch(baseServiceUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
        },
        body: JSON.stringify(data),
      });
      if (!response.ok) {
        throw new Error(response.statusText);
      }

      return true;
    } catch (error) {
      throw new Error(error.message);
    }
  },

  getService: async (id) => {
    try {
      const response = await fetch(`${baseServiceUrl}/${id}`);

      if (!response.ok) {
        throw new Error(response.statusText);
      }

      return response.json();
    } catch (error) {
      throw new Error(error.message);
    }
  },
};

export default api;
