import React from "react";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

/**
 * Компонент иконки
 *
 * @param name - название иконки
 * @returns {JSX.Element}
 * @constructor
 */
function Icon({ name, ...props }) {
  return <FontAwesomeIcon icon={name} {...props} />;
}

export default Icon;
