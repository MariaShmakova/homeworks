import React from "react";

function LoaderXs() {
  return (
    <span
      className="spinner-border spinner-border-sm"
      role="status"
      aria-hidden="true"
    />
  );
}

export default LoaderXs;
