import React from "react";

function Error({ text }) {
  return <div className="alert alert-danger">{text}</div>;
}

export default Error;
