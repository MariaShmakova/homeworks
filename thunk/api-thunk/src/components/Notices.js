import React from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import "moment/locale/ru";
import { removeNotice } from "../store/modules/notices";

function Notices() {
  const { list } = useSelector((state) => state.notices);
  const dispatch = useDispatch();

  const handleRemove = (id) => {
    dispatch(removeNotice(id));
  };

  if (!list || !list.length) {
    return <></>;
  }

  return (
    <div className="toast-container position-fixed bottom-0 start-0 p-3">
      {list.map((item) => (
        <div
          key={item.id}
          className="toast show"
          role="alert"
          aria-live="assertive"
          aria-atomic="true"
        >
          <div className="toast-header">
            <strong className="me-auto text-danger">
              {item.type === "error" ? "Ошибка" : "Уведомление"}
            </strong>
            <small className="text-muted">
              {moment(item.created_at).fromNow()}
            </small>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="toast"
              aria-label="Close"
              onClick={() => handleRemove(item.id)}
            />
          </div>
          <div className="toast-body">{item.message}</div>
        </div>
      ))}
    </div>
  );
}

export default Notices;
