import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouteMatch, useHistory } from "react-router-dom";
import {
  saveService,
  fetchService,
  serviceClear,
  serviceFormFieldChange,
} from "../store/modules/serviceForm";
import { LoaderXs } from "./Loader";
import Error from "./Error";

/**
 * Форма для добавления/редактирования сервиса
 *
 * @returns {JSX.Element}
 * @constructor
 */
function ServiceForm() {
  const match = useRouteMatch();
  const { loading, form, error } = useSelector((state) => state.serviceForm);
  const id = +match.params.id || 0;

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (id) {
      dispatch(fetchService(id));
    }
  }, [id, dispatch]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(saveService(form));
  };

  const handleChange = (e) => {
    const { value, name } = e.target;
    dispatch(serviceFormFieldChange({ name, value }));
  };

  const handleCancel = (e) => {
    e.preventDefault();
    dispatch(serviceClear());
    history.push("/");
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-3">
        <input
          className="form-control"
          type="text"
          id="name"
          name="name"
          required
          placeholder="Название услуги"
          value={form.name}
          onChange={handleChange}
          disabled={loading}
        />
      </div>
      <div className="mb-3">
        <input
          className="form-control"
          type="number"
          id="price"
          name="price"
          required
          placeholder="Цена"
          value={form.price}
          onChange={handleChange}
          disabled={loading}
        />
      </div>
      <div className="mb-3">
        <input
          className="form-control"
          type="text"
          id="content"
          name="content"
          required
          placeholder="Описание"
          value={form.content}
          onChange={handleChange}
          disabled={loading}
        />
      </div>
      <div className="btn-group mb-3">
        <button type="submit" className="btn btn-primary" disabled={loading}>
          {loading ? <LoaderXs /> : "Сохранить"}
        </button>
        {!!form.id && (
          <button
            className="btn btn-outline-primary"
            disabled={loading}
            onClick={handleCancel}
          >
            Отмена
          </button>
        )}
      </div>
      {error && <Error text="Произошла ошибка! Повторите запрос." />}
    </form>
  );
}

export default ServiceForm;
