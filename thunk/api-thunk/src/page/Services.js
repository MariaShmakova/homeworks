import React from "react";
import ServiceForm from "../components/ServiceForm";
import ServiceFilter from "../components/ServiceFilter";
import ServiceList from "../components/ServiceList";

function Services() {
  return (
    <div className="row">
      <div className="col">
        <h2>Добавить услугу</h2>
        <ServiceForm />
      </div>
      <div className="col">
        <h2>Список услуг</h2>
        <ServiceFilter className="mb-3" />
        <ServiceList />
      </div>
    </div>
  );
}

export default Services;
