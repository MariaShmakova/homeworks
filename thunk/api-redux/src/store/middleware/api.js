import { ACTION_TYPES } from "../modules";
import { apiSuccess, apiFailed } from "../modules/api/actions";
import tryParseJSON from "../../utils/tryParseJSON";

const api = ({ dispatch }) => (next) => async (action) => {
  if (action.type !== ACTION_TYPES.API.API_START) return next(action);

  const {
    url,
    method = "GET",
    data,
    onStart,
    onSuccess,
    onError,
    onFinally,
    nameRequest,
  } = action.payload;

  if (onStart) dispatch(onStart());

  next(action);

  try {
    const response = await fetch(`${process.env.REACT_APP_URL}${url}`, {
      method,
      headers: {
        "Content-Type": "application/json;charset=utf-8",
      },
      body: data ? JSON.stringify(data) : null,
    });

    if (!response.ok) {
      throw new Error(response.statusText);
    }

    const text = await response.text();
    const responseData = tryParseJSON(text) || undefined;

    dispatch(apiSuccess(responseData));

    if (onSuccess) dispatch(onSuccess(responseData));
  } catch (error) {
    dispatch(
      apiFailed(
        `Произошла ошибка в процессе выполнения запроса${
          nameRequest ? `: ${nameRequest.toLowerCase()}!` : "!"
        } Повторите запрос позже.`
      )
    );

    if (onError) dispatch(onError(error.message));
  }

  if (onFinally) dispatch(onFinally());
};

export default api;
