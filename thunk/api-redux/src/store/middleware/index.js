import func from "./func";
import api from "./api";
import notice from "./notice";

const middleware = [func, api, notice];
export default middleware;
