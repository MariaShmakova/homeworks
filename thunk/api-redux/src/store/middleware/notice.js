import { ACTION_TYPES } from "../modules";
import { addErrorNotice } from "../modules/notice/actions";

const notice = ({ dispatch }) => (next) => (action) => {
  if (action.type === ACTION_TYPES.API.API_FAILED) {
    dispatch(addErrorNotice(action.payload.error));
  } else return next(action);
};

export default notice;
