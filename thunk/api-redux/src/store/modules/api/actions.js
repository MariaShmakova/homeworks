import ACTIONS from "./actionTypes";

export const apiStart = (payload) => ({
  type: ACTIONS.API_START,
  payload,
});

export const apiFailed = (error) => ({
  type: ACTIONS.API_FAILED,
  payload: { error },
});

export const apiSuccess = (data) => ({
  type: ACTIONS.API_SUCCESS,
  payload: { data },
});
