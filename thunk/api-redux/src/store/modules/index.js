import { combineReducers } from "redux";
import serviceListReducer from "./serviceList/reducer";
import serviceFormReducer from "./serviceForm/reducer";
import noticeReducer from "./notice/reducer";

import API from "./api/actionTypes";
import SERVICE_LIST from "./serviceList/actionTypes";
import SERVICE_FORM from "./serviceForm/actionTypes";
import NOTICE from "./notice/actionTypes";

const reducers = combineReducers({
  serviceList: serviceListReducer,
  serviceForm: serviceFormReducer,
  notices: noticeReducer,
});

const ACTION_TYPES = {
  API,
  SERVICE_LIST,
  SERVICE_FORM,
  NOTICE,
};
export { reducers, ACTION_TYPES };
