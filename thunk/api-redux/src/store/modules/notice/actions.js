import ACTIONS from "./actionTypes";

export const addErrorNotice = (error) => ({
  type: ACTIONS.ADD_NOTICE,
  payload: {
    type: "error",
    message: error,
  },
});

export const removeNotice = (id) => ({
  type: ACTIONS.REMOVE_NOTICE,
  payload: { id },
});
