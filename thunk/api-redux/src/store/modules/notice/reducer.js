import { nanoid } from "nanoid";
import ACTIONS from "./actionTypes";

const INITIAL_STATE = {
  list: [],
};

export default function noticeReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ACTIONS.ADD_NOTICE: {
      const { message, type } = action.payload;
      return {
        ...state,
        list: [
          ...state.list,
          { id: nanoid(), message, type, created_at: new Date() },
        ],
      };
    }

    case ACTIONS.REMOVE_NOTICE: {
      return {
        ...state,
        list: state.list.filter((item) => item.id !== action.payload.id),
      };
    }

    default:
      return state;
  }
}
