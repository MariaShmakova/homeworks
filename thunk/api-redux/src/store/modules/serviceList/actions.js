import ACTIONS from "./actionTypes";
import { apiStart } from "../api/actions";

const base_service_url = "/services";

const changeFilter = (filter) => ({
  type: ACTIONS.CHANGE_FILTER,
  payload: { filter },
});

const serviceRequested = () => ({
  type: ACTIONS.SERVICES_REQUESTED,
});

const serviceReceived = (data) => ({
  type: ACTIONS.SERVICES_RECEIVED,
  payload: { data },
});

const serviceRequestFailed = (error) => ({
  type: ACTIONS.SERVICES_REQUEST_FAILED,
  payload: { error },
});

const loadServices = () => (dispatch) => {
  return dispatch(
    apiStart({
      nameRequest: "Загрузка списка услуг",
      url: base_service_url,
      onStart: serviceRequested,
      onSuccess: serviceReceived,
      onError: serviceRequestFailed,
    })
  );
};

const serviceRemoveStart = (id) => () => ({
  type: ACTIONS.SERVICE_REMOVE_START,
  payload: { id },
});

const serviceRemoved = (id) => () => ({
  type: ACTIONS.SERVICE_REMOVED,
  payload: { id },
});

const serviceRemoveEnd = (id) => () => ({
  type: ACTIONS.SERVICE_REMOVE_END,
  payload: { id },
});

const removeService = (id) => (dispatch) => {
  return dispatch(
    apiStart({
      nameRequest: "Удаление услуги",
      url: `${base_service_url}/${id}`,
      method: "DELETE",
      onStart: serviceRemoveStart(id),
      onSuccess: serviceRemoved(id),
      onFinally: serviceRemoveEnd(id),
    })
  );
};

export { removeService, loadServices, changeFilter };
