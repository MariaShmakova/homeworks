import ACTIONS from "./actionTypes";

const INITIAL_STATE = {
  list: [],
  loading: false,
  error: null,
  filter: "",
  removing_list: [],
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ACTIONS.SERVICES_REQUESTED: {
      return { ...state, loading: true };
    }
    case ACTIONS.SERVICES_RECEIVED: {
      return {
        ...state,
        list: action.payload.data,
        loading: false,
        error: null,
      };
    }
    case ACTIONS.SERVICES_REQUEST_FAILED: {
      return {
        ...state,
        error: action.payload.error,
        loading: false,
      };
    }

    case ACTIONS.SERVICE_REMOVE_START: {
      return {
        ...state,
        removing_list: [...state.removing_list, action.payload.id],
      };
    }
    case ACTIONS.SERVICE_REMOVED:
      return {
        ...state,
        list: state.list.filter((item) => item.id !== action.payload.id),
      };
    case ACTIONS.SERVICE_REMOVE_END: {
      return {
        ...state,
        removing_list: state.removing_list.filter(
          (item) => item !== action.payload.id
        ),
      };
    }
    case ACTIONS.CHANGE_FILTER:
      return { ...state, filter: action.payload.filter };
    default:
      return state;
  }
}
