import ACTIONS from "./actionTypes";
import { apiStart } from "../api/actions";
import { loadServices } from "../serviceList/actions";

const base_service_url = "/services";

const serviceSaveStart = () => ({
  type: ACTIONS.SERVICE_SAVE_START,
  payload: null,
});

const serviceSaveEnd = () => ({
  type: ACTIONS.SERVICE_SAVE_END,
  payload: null,
});

const serviceSaveFailed = (error) => ({
  type: ACTIONS.SERVICE_SAVE_FAILED,
  payload: { error },
});

const serviceClear = () => ({
  type: ACTIONS.SERVICE_CLEAR,
  payload: null,
});

const serviceFormFieldChanged = ({ name, value }) => ({
  type: ACTIONS.SERVICE_FORM_FIELD_CHANGED,
  payload: { name, value },
});

const serviceCreated = () => (dispatch) => {
  dispatch(loadServices());
  dispatch(serviceClear());
};

const serviceUpdated = () => (dispatch) => {
  // TODO: Help Не понимаю как реализовать через history
  window.location.href = "/";
};

const saveService = (data) => (dispatch) => {
  return dispatch(
    apiStart({
      nameRequest: "Сохранение услуги",
      url: base_service_url,
      method: "POST",
      data,
      onStart: serviceSaveStart,
      onSuccess: data.id === 0 ? serviceCreated : serviceUpdated,
      onError: serviceSaveFailed,
      onFinally: serviceSaveEnd,
    })
  );
};

const serviceLoaded = (data) => ({
  type: ACTIONS.SERVICE_LOADED,
  payload: { data },
});

const getService = (id) => (dispatch) => {
  return dispatch(
    apiStart({
      nameRequest: "Зазгрузка данных услуги",
      url: base_service_url + "/" + id,
      onStart: serviceSaveStart,
      onSuccess: serviceLoaded,
      onError: serviceSaveFailed,
      onFinally: serviceSaveEnd,
    })
  );
};

export { getService, saveService, serviceFormFieldChanged, serviceClear };
