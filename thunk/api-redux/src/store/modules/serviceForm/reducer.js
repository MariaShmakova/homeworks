import ACTIONS from "./actionTypes";

const INITIAL_STATE = {
  form: {
    id: 0,
    name: "",
    price: "",
    content: "",
  },
  loading: false,
  error: null,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ACTIONS.SERVICE_SAVE_START: {
      return { ...state, loading: true };
    }
    case ACTIONS.SERVICE_LOADED: {
      return { ...state, form: { ...action.payload.data } };
    }

    case ACTIONS.SERVICE_FORM_FIELD_CHANGED: {
      const { name, value } = action.payload;
      return { ...state, form: { ...state.form, [name]: value } };
    }

    case ACTIONS.SERVICE_SAVE_END: {
      return { ...state, loading: false };
    }

    case ACTIONS.SERVICE_SAVE_FAILED: {
      return { ...state, error: action.payload.error };
    }

    case ACTIONS.SERVICE_CLEAR: {
      return { ...state, form: { ...INITIAL_STATE.form }, error: null };
    }
    default:
      return state;
  }
}
