export default function formatCurrency(number) {
  if (isNaN(number)) {
    return false;
  }

  return Number(number).toLocaleString("ru-RU", {
    style: "currency",
    currency: "RUB",
    minimumFractionDigits: 0,
  });
}
