import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import {
  loadServices,
  removeService,
} from "../store/modules/serviceList/actions";
import Icon from "./Icon";
import Loader, { LoaderXs } from "./Loader";
import Error from "./Error";
import formatCurrency from "../utils/formatCurrency";

/**
 * Список сервисов
 *
 * @returns {JSX.Element}
 * @constructor
 */
function ServiceList() {
  const dispatch = useDispatch();
  const { list, loading, error, filter, removing_list } = useSelector(
    (state) => state.serviceList
  );

  const [filteredList, setFilteredList] = useState([]);

  useEffect(() => {
    dispatch(loadServices());
  }, [dispatch]);

  useEffect(() => {
    if (!filter || !filter.length) {
      return setFilteredList(list);
    }

    setFilteredList(
      list.filter(
        (item) => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
      )
    );
  }, [filter, list]);

  const handleRemove = (id) => {
    dispatch(removeService(id));
  };

  const EmptyList = () => {
    if (filter && filter.length) {
      return <span>Под фильтр "{filter}" не подходит ни одна услуга!</span>;
    }
    return <span>Список услуг пуст!</span>;
  };

  const renderServiceActions = (id) => {
    if (removing_list.includes(id)) {
      return (
        <button
          className="btn btn-outline-danger float-end"
          disabled
          title="Удалить"
        >
          <LoaderXs />
        </button>
      );
    }

    return (
      <span className="btn-group float-end">
        <Link
          to={`/services/${id}`}
          className="btn btn-success"
          title="Редактировать"
        >
          <Icon name="edit" />
        </Link>
        <button
          className="btn btn-outline-danger"
          onClick={() => handleRemove(id)}
          title="Удалить"
        >
          <Icon name="trash" />
        </button>
      </span>
    );
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <Error text="Произошла ошибка!" />;
  }

  return (
    <div className="service-list">
      <ul className="list-group">
        {filteredList.map((item) => (
          <li key={item.id} className="list-group-item">
            {item.name}: {formatCurrency(item.price)}
            {renderServiceActions(item.id)}
          </li>
        ))}
      </ul>
      {!filteredList.length && <EmptyList />}
    </div>
  );
}

export default ServiceList;
