import React from "react";
import "./CardImg.css";

function CardImg({ src, alt }) {
  return (
    <img
      src={`${process.env.REACT_APP_IMG}${src}`}
      className="CardImg card-img-top"
      alt={alt}
    />
  );
}

export default CardImg;
