import React from "react";
import Card from "../Card/Card";
import CardImg from "../CardImg/CardImg";
import "./App.css";

function App() {
  const cards = [
    {
      id: "1",
      title: "Card title",
      text: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi
          assumenda atque cumque eos, illo laboriosam libero magni, minima neque
          nihil odit officiis, praesentium quia repellat vitae voluptas
          voluptatem. Vero!`,
      img: null,
    },
    {
      id: "2",
      title: "Card title 2",
      text: `Test Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi
          assumenda atque cumque eos, illo laboriosam libero magni, minima neque
          nihil odit officiis, praesentium quia repellat vitae voluptas
          voluptatem. Vero!`,
      img: "/180",
    },
  ];
  return (
    <div className="App">
      {cards.map((card) => (
        <Card key={card.id} title={card.title} text={card.text}>
          {card.img && <CardImg src={card.img} alt={card.title} />}
        </Card>
      ))}
    </div>
  );
}

export default App;
