import React from "react";

/**
 * Компонент навигации
 *
 * @returns {JSX.Element}
 * @constructor
 */
function Nav() {
  const navs = [];
  for (let i = 0; i < 7; i++) {
    navs.push({
      id: i,
      title: `Link ${i}`,
      url: `/link${i}`,
    });
  }
  return (
    <nav className="nav">
      {navs.map((nav) => (
        <a key={nav.id} className="nav-link" href={nav.url}>
          {nav.title}
        </a>
      ))}
    </nav>
  );
}

export default Nav;
