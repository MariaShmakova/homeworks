import React from "react";
import moment from "moment";

DateTime.defaultProps = {
  format: "DD MMMM, ddd HH mm",
};

/**
 * Текущая дата
 *
 * @param format
 * @returns {JSX.Element}
 * @constructor
 */
function DateTime({ format }) {
  const date = moment().format(format);

  return <p className="text-muted">{date}</p>;
}

export default DateTime;
