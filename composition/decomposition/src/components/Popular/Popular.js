import React from "react";

/**
 * Блок посещаемое
 *
 * @returns {JSX.Element}
 * @constructor
 */
function Popular() {
  const items = [];

  for (let i = 0; i < 7; i++) {
    items.push({
      id: i,
      title: `Popular ${i}`,
      url: `/popular${i}`,
    });
  }

  return (
    <ul>
      {items.map((item) => (
        <li key={item.id}>
          <a href={item.url}>{item.title}</a>
        </li>
      ))}
    </ul>
  );
}

export default Popular;
