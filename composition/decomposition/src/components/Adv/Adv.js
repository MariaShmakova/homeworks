import React from "react";
import "./Adv.css";

/**
 * Блок рекламы
 *
 * @returns {JSX.Element}
 * @constructor
 */
function Adv() {
  return (
    <div className="Adv">
      <img src="https://via.placeholder.com/1000x100" alt="Adv" />
    </div>
  );
}

export default Adv;
