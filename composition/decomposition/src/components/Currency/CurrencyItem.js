import React from "react";

/**
 * Валюта
 *
 * @param name - название валюты
 * @param value - величина
 * @param delta - рост/падение валюты
 * @returns {JSX.Element}
 * @constructor
 */
function CurrencyItem({ name, value, delta }) {
  return (
    <div className="CurrencyItem me-2">
      <strong>{name}</strong>
      <span>{value}</span>
      <small className="text-muted">delta</small>
    </div>
  );
}

export default CurrencyItem;
