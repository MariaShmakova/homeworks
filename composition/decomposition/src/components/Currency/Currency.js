import React from "react";
import CurrencyItem from "./CurrencyItem";
import "./Currency.css";

/**
 * Список валют
 *
 * @returns {JSX.Element}
 * @constructor
 */
function Currency() {
  const currency = [
    {
      name: "USD",
      value: "63,52",
      delta: "+0,09",
    },
    {
      name: "RUR",
      value: "44,52",
      delta: "+0,19",
    },
  ];
  return (
    <div className="Currency">
      {currency.map((item) => (
        <CurrencyItem key={item.name} {...item} />
      ))}
    </div>
  );
}

export default Currency;
