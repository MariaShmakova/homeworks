import React from "react";
import InfoBlock from "../InfoBlock/InfoBlock";

/**
 * Случайный информационный блок
 *
 * @returns {JSX.Element}
 * @constructor
 */
function RandomInfoBlock() {
  return (
    <InfoBlock
      title="Работа над ошибками"
      img="https://via.placeholder.com/150"
      url="/test"
    >
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
        consequatur dignissimos dolores est expedita maiores nemo perferendis
        quasi sit vel. Culpa delectus doloribus dolorum eveniet fugit inventore
        nobis recusandae tempora.
      </p>
    </InfoBlock>
  );
}

export default RandomInfoBlock;
