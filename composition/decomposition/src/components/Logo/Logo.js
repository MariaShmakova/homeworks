import React from "react";
import Icon from "../Icon/Icon";

/**
 * Логотип
 *
 * @returns {JSX.Element}
 * @constructor
 */
function Logo() {
  return (
    <div className="Logo">
      <Icon name="apple-alt" size="6x" />
    </div>
  );
}

export default Logo;
