import React from "react";
import InfoBlock from "../InfoBlock/InfoBlock";
import Popular from "../Popular/Popular";

/**
 * Список информационных блоков
 *
 * @returns {JSX.Element}
 * @constructor
 */
function InfoList() {
  return (
    <div className="InfoList row">
      <div className="col-md-4">
        <InfoBlock title="Погода" url="/weather">
          Погода
        </InfoBlock>
      </div>
      <div className="col-md-4">
        <InfoBlock url="/popular" title="Посещаемое">
          <Popular />
        </InfoBlock>
      </div>
      <div className="col-md-4">
        <InfoBlock url="/map" title="Карта">
          Карта
        </InfoBlock>
      </div>
      <div className="col-md-4">
        <InfoBlock url="/tv" title="Телепрограмма">
          Телепрограмма
        </InfoBlock>
      </div>
      <div className="col-md-4">
        <InfoBlock url="/qwerty" title="Эфир">
          Эфир
        </InfoBlock>
      </div>
    </div>
  );
}

export default InfoList;
