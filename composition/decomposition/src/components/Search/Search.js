import React from "react";

function Search() {
  return (
    <div className="Search">
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Recipient's username"
        />
        <button className="btn btn-outline-secondary" type="button">
          Найти
        </button>
      </div>
    </div>
  );
}

export default Search;
