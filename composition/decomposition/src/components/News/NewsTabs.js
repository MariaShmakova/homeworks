import React from "react";
import PropTypes from "prop-types";

NewsTabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
    })
  ).isRequired,
  onSelectTab: PropTypes.func.isRequired,
};

/**
 * Компонент для переключения темы новостей
 *
 * @param tabs
 * @param activeTab
 * @param onSelectTab
 * @returns {JSX.Element}
 * @constructor
 */
function NewsTabs({ tabs, activeTab, onSelectTab }) {
  return (
    <ul className="NewsTab nav nav-pills">
      {tabs.map((tab) => (
        <li key={tab.id} className="nav-item">
          <a
            href="#"
            className={`nav-link ${activeTab === tab.id ? "active" : ""}`}
            onClick={() => onSelectTab(tab.id)}
          >
            {tab.title}
          </a>
        </li>
      ))}
    </ul>
  );
}

export default NewsTabs;
