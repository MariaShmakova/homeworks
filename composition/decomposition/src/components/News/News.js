import React from "react";
import NewsTabs from "./NewsTabs";
import NewsList from "./NewsList";
import DateTime from "../DateTime/DateTime";

/**
 * Блок новостей
 *
 * @returns {JSX.Element}
 * @constructor
 */
function News() {
  const activeTab = "1";
  const tabs = [
    {
      id: "1",
      title: "Tab1",
    },
    {
      id: "2",
      title: "Tab2",
    },
  ];
  const news = [
    {
      id: "1",
      icon: "newspaper",
      text: "Test news text",
      url: "/1",
    },
    {
      id: "2",
      icon: "newspaper",
      text: "Test news text 2",
      url: "/2",
    },
    {
      id: "3",
      icon: "newspaper",
      text: "Test news text 3",
      url: "/3",
    },
    {
      id: "4",
      icon: "newspaper",
      text: "Test news text 4",
      url: "/4",
    },
    {
      id: "5",
      icon: "newspaper",
      text: "Test news text 5",
      url: "/5",
    },
  ];
  const onSelectTab = () => {};

  return (
    <div className="News row">
      <div className="col-md-8">
        <NewsTabs tabs={tabs} activeTab={activeTab} onSelectTab={onSelectTab} />
      </div>
      <div className="col-md-4">
        <DateTime />
      </div>
      <div className="col-md-12">
        <NewsList news={news} />
      </div>
    </div>
  );
}

export default News;
