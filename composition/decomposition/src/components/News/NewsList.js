import React from "react";
import NewsItem from "./NewsItem";
import PropTypes from "prop-types";

NewsList.propTypes = {
  news: PropTypes.array.isRequired,
};

/**
 * Компонент списка новостей
 *
 * @param news - список новости
 * @returns {JSX.Element}
 * @constructor
 */
function NewsList({ news }) {
  return (
    <ul className="NewsList list-group list-group-flush">
      {news.map((item) => {
        const { id, ...data } = item;
        return (
          <li key={id} className="list-group-item">
            <NewsItem {...data} />
          </li>
        );
      })}
    </ul>
  );
}

export default NewsList;
