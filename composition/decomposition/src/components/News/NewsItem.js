import React from "react";
import Icon from "../Icon/Icon";
import PropTypes from "prop-types";

NewsItem.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

/**
 * Компонент новости
 *
 * @param icon - иконка новости
 * @param text - текст новости
 * @param url - ссылка на новость
 * @returns {JSX.Element}
 * @constructor
 */

function NewsItem({ icon, text, url }) {
  return (
    <a href={url}>
      {icon && <Icon name={icon} />}
      {text}
    </a>
  );
}

export default NewsItem;
