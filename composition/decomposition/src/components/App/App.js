import React from "react";
import News from "../News/News";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Currency from "../Currency/Currency";
import RandomInfoBlock from "../RandomInfoBlock/RandomInfoBlock";
import Nav from "../Nav/Nav";
import Logo from "../Logo/Logo";
import Search from "../Search/Search";
import Adv from "../Adv/Adv";
import InfoList from "../InfoList/InfoList";

function App() {
  return (
    <div className="App container mt-4">
      <div className="row">
        <div className="col-md-8">
          <News />
          <Currency />
        </div>
        <div className="col-md-4">
          <RandomInfoBlock />
        </div>
      </div>
      <section className="row">
        <div className="col-md-12">
          <Nav />
        </div>
      </section>
      <section className="row">
        <div className="col-md-2">
          <Logo />
        </div>
        <div className="col-md-10">
          <Search />
        </div>
      </section>
      <section className="row">
        <div className="col-md-12">
          <Adv />
        </div>
      </section>
      <section className="row">
        <div className="col-md-12">
          <InfoList />
        </div>
      </section>
    </div>
  );
}

export default App;
