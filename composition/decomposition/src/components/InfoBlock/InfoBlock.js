import React from "react";
import PropTypes from "prop-types";
import "./InfoBlock.css";

InfoBlock.propTypes = {
  title: PropTypes.string.isRequired,
  img: PropTypes.string,
  url: PropTypes.string.isRequired,
};

/**
 * Компонент информационного блока
 *
 * @param title
 * @param img
 * @param url
 * @param children
 * @returns {JSX.Element}
 * @constructor
 */
function InfoBlock({ title, img, url, children }) {
  return (
    <div className="InfoBlock card">
      {img && (
        <img src={img} alt={title} className="InfoBlock-img card-img-top" />
      )}
      <div className="card-body">
        <a href={url}>
          <h5 className="card-title">{title}</h5>
        </a>
        {children}
      </div>
    </div>
  );
}

export default InfoBlock;
