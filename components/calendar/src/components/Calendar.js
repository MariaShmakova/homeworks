import React from "react";
import PropTypes from "prop-types";
import CalendarHeader from "./Calendar/CalendarHeader";
import CalendarTable from "./Calendar/CalendarTable";

function Calendar({ date }) {
  return (
    <div>
      <div className="ui-datepicker">
        <CalendarHeader date={date} />
        <CalendarTable date={date} />
      </div>
    </div>
  );
}

Calendar.propTypes = {
  date: PropTypes.instanceOf(Date).isRequired,
};

export default Calendar;
