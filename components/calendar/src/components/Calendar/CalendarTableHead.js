import React from "react";
import moment from "moment";
import "moment/locale/ru";

function CalendarTableHead() {
  moment.locale("ru");

  const week = [1, 2, 3, 4, 5, 6, 7].map((item) => {
    const dayOfWeek = moment().day(item);
    return {
      value: dayOfWeek.format("dd"),
      title: dayOfWeek.format("dddd"),
    };
  });

  return (
    <>
      <colgroup>
        <col />
        <col />
        <col />
        <col />
        <col />
        <col className="ui-datepicker-week-end" />
        <col className="ui-datepicker-week-end" />
      </colgroup>
      <thead>
        <tr>
          {week.map(({ title, value }) => (
            <th scope="col" title={title} key={value}>
              {value}
            </th>
          ))}
        </tr>
      </thead>
    </>
  );
}

export default CalendarTableHead;
