import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import "moment/locale/ru";

function CalendarHeader(props) {
  moment.locale("ru");

  const now = moment(props.date);
  const data = {
    day: now.date(),
    dayOfWeek: now.format("dddd"),
    month: now.format("MMMM"),
    monthInGenitive: now.format("D MMMM").split(" ")[1],
    year: now.year(),
  };

  return (
    <div>
      <div className="ui-datepicker-material-header">
        <div className="ui-datepicker-material-day">{data.dayOfWeek}</div>
        <div className="ui-datepicker-material-date">
          <div className="ui-datepicker-material-day-num">{data.day}</div>
          <div className="ui-datepicker-material-month">
            {data.monthInGenitive}
          </div>
          <div className="ui-datepicker-material-year">{data.year}</div>
        </div>
      </div>
      <div className="ui-datepicker-header">
        <div className="ui-datepicker-title">
          <span className="ui-datepicker-month">{data.month}</span>&nbsp;
          <span className="ui-datepicker-year">{data.year}</span>
        </div>
      </div>
    </div>
  );
}

CalendarHeader.propTypes = {
  date: PropTypes.instanceOf(Date).isRequired,
};

export default CalendarHeader;
