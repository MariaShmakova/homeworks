import React from "react";
import PropTypes from "prop-types";

function CalendarDay({ isCurrent, isActive, value }) {
  return (
    <td
      className={`
        ${!isCurrent && "ui-datepicker-other-month"}
        ${isActive && "ui-datepicker-today"}
      `}
    >
      {value}
    </td>
  );
}

CalendarDay.propTypes = {
  isCurrent: PropTypes.bool,
  isActive: PropTypes.bool,
  value: PropTypes.number.isRequired,
};

CalendarDay.defaultProps = {
  isCurrent: false,
  isActive: false,
};

export default CalendarDay;
