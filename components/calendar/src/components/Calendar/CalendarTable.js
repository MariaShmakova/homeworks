import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import CalendarDay from "./CalendarDay";
import CalendarTableHead from "./CalendarTableHead";

function CalendarTable({ date }) {
  const days = []
    .concat(getDaysFromPrevMonth(date))
    .concat(getDaysFromCurrentMonth(date))
    .concat(getDaysFromNextMonth(date));
    
  const weeks = breakDaysIntoWeeks(days);

  return (
    <table className="ui-datepicker-calendar">
      <CalendarTableHead />
      <tbody>
        {weeks.map(({ week, id }) => (
          <tr key={id}>
            {week.map((day) => (
              <CalendarDay key={day.id} {...day} />
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
}

function getDaysFromPrevMonth(date) {
  const result = [];
  const firstDayOfWeekForCurrentMonth = moment(date).startOf("month").day();
  const countDayInPrevMonth = moment(date).subtract(1, "month").daysInMonth();

  for (let i = firstDayOfWeekForCurrentMonth - 2; i >= 0; i--) {
    result.push({
      id: `prev${i}`,
      value: countDayInPrevMonth - i,
      isCurrent: false,
    });
  }

  return result;
}

function getDaysFromCurrentMonth(date) {
  const result = [];
  const countDayInCurrentMonth = moment(date).daysInMonth();

  for (let i = 1; i <= countDayInCurrentMonth; i++) {
    result.push({
      id: `current${i}`,
      value: i,
      isCurrent: true,
      isActive: i === moment(date).date(),
    });
  }

  return result;
}

function getDaysFromNextMonth(date) {
  const result = [];
  const lastDayOfWeekForCurrentMonth = moment(date).endOf("month").day();

  if (lastDayOfWeekForCurrentMonth === 0) {
    return result;
  }

  for (let i = 1; i < 7 - (lastDayOfWeekForCurrentMonth - 1); i++) {
    result.push({
      id: `next${i}`,
      value: i,
      isCurrent: false,
    });
  }

  return result;
}

function breakDaysIntoWeeks(days) {
  const countWeek = Math.ceil(days.length / 7);
  const weeks = [];

  for (let i = 0; i < countWeek; i++) {
    const week = days.slice(i * 7, 7 * (i + 1));
    weeks.push({
      id: i,
      week,
    });
  }

  return weeks;
}

CalendarTable.propTypes = {
  date: PropTypes.instanceOf(Date).isRequired,
};

export default CalendarTable;
