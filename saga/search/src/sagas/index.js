import { spawn } from "redux-saga/effects";
import watchSearchSkillsSaga from "./watchSearchSkillsSaga";
import watchChangeSearchSaga from "./watchChangeSearchSaga";

export default function* saga() {
  yield spawn(watchChangeSearchSaga);
  yield spawn(watchSearchSkillsSaga);
}
