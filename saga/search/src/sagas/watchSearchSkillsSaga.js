import { put, retry, takeLatest } from "redux-saga/effects";
import { searchSkills } from "../api";
import {
  searchSkillsFailure,
  searchSkillsSuccess,
} from "../actions/actionCreators";
import { SEARCH_SKILLS_REQUEST } from "../actions/actionTypes";

function* handleSearchSkillsSaga(action) {
  try {
    const retryCount = 3;
    const retryDelay = 1 * 1000; // ms
    const data = yield retry(
      retryCount,
      retryDelay,
      searchSkills,
      action.payload.search
    );
    yield put(searchSkillsSuccess(data));
  } catch (e) {
    yield put(searchSkillsFailure(e.message));
  }
}

function* watchSearchSkillsSaga() {
  yield takeLatest(SEARCH_SKILLS_REQUEST, handleSearchSkillsSaga);
}

export default watchSearchSkillsSaga;
