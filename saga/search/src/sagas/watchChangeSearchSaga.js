import { CHANGE_SEARCH_FIELD } from "../actions/actionTypes";
import { debounce, put } from "redux-saga/effects";
import { clearSkills, searchSkillsRequest } from "../actions/actionCreators";

function filterChangeSearchAction({ type }) {
  return type === CHANGE_SEARCH_FIELD;
}

function* handleChangeSearchSaga(action) {
  const search = action.payload.search.trim();
  yield put(search !== "" ? searchSkillsRequest(search) : clearSkills());
}

function* watchChangeSearchSaga() {
  yield debounce(100, filterChangeSearchAction, handleChangeSearchSaga);
}

export default watchChangeSearchSaga;
