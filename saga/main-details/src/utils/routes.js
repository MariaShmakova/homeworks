const baseServiceUrl = `${process.env.REACT_APP_URL}/services`;

const routes = {
  getServices: baseServiceUrl,
  getService: (id) => `${baseServiceUrl}/${id}`,
};

export default routes;
