import { put, call, takeLatest } from "redux-saga/effects";
import {
  fetchServiceDetailFailed,
  fetchServiceDetailRequest,
  fetchServiceDetailSuccess,
} from "../modules/serviceForm";
import api from "../../services/ApiService";

function* handleFetchServiceSaga(action) {
  try {
    const data = yield call(api.getService, action.payload);
    yield put(fetchServiceDetailSuccess(data));
  } catch (e) {
    yield put(fetchServiceDetailFailed(e.message));
  }
}

function* watchFetchServiceSaga() {
  yield takeLatest(fetchServiceDetailRequest.type, handleFetchServiceSaga);
}

export default watchFetchServiceSaga;
