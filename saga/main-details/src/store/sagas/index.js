import { spawn } from "redux-saga/effects";
import fetchServicesSaga from "./fetchServicesSaga";
import fetchServiceSaga from "./fetchServiceSaga";

export default function* saga() {
  yield spawn(fetchServicesSaga);
  yield spawn(fetchServiceSaga);
}
