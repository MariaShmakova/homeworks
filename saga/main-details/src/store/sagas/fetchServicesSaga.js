import { put, call, takeLatest } from "redux-saga/effects";
import {
  fetchServicesRequest,
  fetchServicesFailed,
  fetchServicesSuccess,
} from "../modules/services";
import api from "../../services/ApiService";

function* handleFetchServicesSaga(action) {
  try {
    const data = yield call(api.getServices);
    yield put(fetchServicesSuccess(data));
  } catch (e) {
    yield put(fetchServicesFailed(e.message));
  }
}

function* watchFetchServicesSaga() {
  yield takeLatest(fetchServicesRequest.type, handleFetchServicesSaga);
}

export default watchFetchServicesSaga;
