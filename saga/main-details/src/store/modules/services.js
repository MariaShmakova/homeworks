import { createSlice } from "@reduxjs/toolkit";
import { createSelector } from "reselect";

const slice = createSlice({
  name: "services",
  initialState: {
    list: [],
    loading: false,
    error: null,
    filter: "",
  },
  reducers: {
    fetchServicesRequest: (services, action) => {
      services.loading = true;
    },
    fetchServicesSuccess: (services, action) => {
      services.list = action.payload;
      services.loading = false;
      services.error = null;
    },
    fetchServicesFailed: (services, action) => {
      services.error = action.payload;
      services.loading = false;
    },
    changeFilter: (services, action) => {
      services.filter = action.payload;
    },
  },
});

const {
  fetchServicesRequest,
  fetchServicesSuccess,
  fetchServicesFailed,
  changeFilter,
} = slice.actions;

const getServicesData = (state) => state.services;

const getFilteredServices = createSelector(
  (state) => state.services.list,
  (state) => state.services.filter,
  (services, filter) => {
    if (!filter || !filter.length) {
      return services;
    }
    return services.filter(
      (item) => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );
  }
);

export {
  changeFilter,
  getServicesData,
  getFilteredServices,
  fetchServicesRequest,
  fetchServicesSuccess,
  fetchServicesFailed,
};

export default slice.reducer;
