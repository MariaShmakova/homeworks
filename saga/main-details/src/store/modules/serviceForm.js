import { createSlice } from "@reduxjs/toolkit";

const INITIAL_STATE = {
  form: {
    id: 0,
    name: "",
    price: "",
    content: "",
  },
  loading: false,
  error: null,
};

const slice = createSlice({
  name: "serviceForm",
  initialState: INITIAL_STATE,
  reducers: {
    fetchServiceDetailRequest: (state) => {
      state.loading = true;
      state.error = null;
    },
    fetchServiceDetailSuccess: (state, action) => {
      state.form = action.payload;
      state.loading = false;
    },
    fetchServiceDetailFailed: (state, action) => {
      state.error = action.payload;
      state.loading = false;
    },
    serviceFormFieldChange: (state, action) => {
      const { name, value } = action.payload;
      state.form[name] = value;
    },

    serviceClear: (state, action) => {
      state.error = null;
      state.form = INITIAL_STATE.form;
    },
  },
});

export const {
  fetchServiceDetailRequest,
  fetchServiceDetailFailed,
  fetchServiceDetailSuccess,
  serviceFormFieldChange,
  serviceClear,
} = slice.actions;

export default slice.reducer;
