import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Services from "./page/Services";
import ServiceItem from "./page/ServiceItem";

function App() {
  return (
    <Router>
      <div className="container p-4">
        <Switch>
          <Route path="/services/:id/details" component={ServiceItem} />
          <Route path="/" component={Services} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
