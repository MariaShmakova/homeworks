const baseServiceUrl = `${process.env.REACT_APP_URL}/services`;
const api = {
  getServices: async () => {
    const response = await fetch(baseServiceUrl);

    if (!response.ok) {
      throw new Error(response.statusText);
    }

    return response.json();
  },
  getService: async (id) => {
    const response = await fetch(`${baseServiceUrl}/${id}`);

    if (!response.ok) {
      throw new Error(response.statusText);
    }

    return response.json();
  },
};

export default api;
