import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import {
  getFilteredServices,
  getServicesData,
  fetchServicesRequest,
} from "../store/modules/services";
import Icon from "./Icon";
import Loader from "./Loader";
import Error from "./Error";
import formatCurrency from "../utils/formatCurrency";

/**
 * Список сервисов
 *
 * @returns {JSX.Element}
 * @constructor
 */
function ServiceList() {
  const dispatch = useDispatch();
  const { loading, error, filter } = useSelector(getServicesData);

  const filteredList = useSelector(getFilteredServices);

  useEffect(() => {
    dispatch(fetchServicesRequest());
  }, [dispatch]);

  const handleRetry = () => {
    dispatch(fetchServicesRequest());
  };

  const EmptyList = () => {
    if (filter && filter.length) {
      return <span>Под фильтр "{filter}" не подходит ни одна услуга!</span>;
    }
    return <span>Список услуг пуст!</span>;
  };

  const renderServiceActions = (id) => {
    return (
      <span className="btn-group float-end">
        <Link
          to={`/services/${id}/details`}
          className="btn btn-success"
          title="Редактировать"
        >
          <Icon name="edit" />
        </Link>
      </span>
    );
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <Error text="Произошла ошибка!" handleRetry={handleRetry} />;
  }

  return (
    <div className="service-list">
      <ul className="list-group">
        {filteredList.map((item) => (
          <li key={item.id} className="list-group-item">
            {item.name}: {formatCurrency(item.price)}
            {renderServiceActions(item.id)}
          </li>
        ))}
      </ul>
      {!filteredList.length && <EmptyList />}
    </div>
  );
}

export default ServiceList;
