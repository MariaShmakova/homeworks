export default function formatCurrency(number, currency = "руб.") {
  if (isNaN(number)) {
    return false;
  }

  return `${Number(number).toLocaleString("ru-RU")} ${currency}`;
}
