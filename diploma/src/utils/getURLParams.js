export default function getQueryURLByParams(params) {
  const query = new URLSearchParams();

  for (let prop in params) {
    if (!params.hasOwnProperty(prop) || !params[prop]) {
      continue;
    }

    query.set(prop, params[prop]);
  }

  return query;
}
