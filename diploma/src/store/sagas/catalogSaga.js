import { takeLatest, put, call } from "redux-saga/effects";
import { fetchRequest, fetchSuccess, fetchFailed } from "../modules/catalog";
import ApiService from "../../services/ApiService";

function* handleCatalogRequestSaga({ payload }) {
  try {
    const data = yield call(ApiService.getItems, payload);
    yield put(fetchSuccess(data));
  } catch (e) {
    yield put(fetchFailed(e.message));
  }
}

function* watchCatalogSaga() {
  yield takeLatest(fetchRequest.type, handleCatalogRequestSaga);
}

export default watchCatalogSaga;
