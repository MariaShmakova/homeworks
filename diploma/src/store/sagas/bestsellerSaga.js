import { takeLatest, put, call } from "redux-saga/effects";
import { fetchFailed, fetchRequest, fetchSuccess } from "../modules/bestseller";
import ApiService from "../../services/ApiService";

function* handleBestsellerRequestSaga() {
  try {
    const data = yield call(ApiService.getTopSales);
    yield put(fetchSuccess(data));
  } catch (e) {
    yield put(fetchFailed(e.message));
  }
}

function* watchBestsellerSaga() {
  yield takeLatest(fetchRequest.type, handleBestsellerRequestSaga);
}

export default watchBestsellerSaga;
