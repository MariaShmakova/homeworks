import bestsellerSaga from "./bestsellerSaga";
import detailSaga from "./detailSaga";
import catalogSaga from "./catalogSaga";
import categoriesSaga from "./categoriesSaga";

export default function* saga() {
  yield* bestsellerSaga();
  yield* detailSaga();
  yield* catalogSaga();
  yield* categoriesSaga();
}
