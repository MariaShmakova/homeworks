import { takeLatest, put, call } from "redux-saga/effects";
import { fetchRequest, fetchSuccess, fetchFailed } from "../modules/detail";
import ApiService from "../../services/ApiService";

function* handleDetailRequestSaga(action) {
  try {
    const data = yield call(ApiService.getItem, action.payload);
    yield put(fetchSuccess(data));
  } catch (e) {
    yield put(fetchFailed(e.message));
  }
}

function* watchDetailSaga() {
  yield takeLatest(fetchRequest.type, handleDetailRequestSaga);
}

export default watchDetailSaga;
