import { takeLatest, put, call } from "redux-saga/effects";
import { fetchFailed, fetchRequest, fetchSuccess } from "../modules/categories";
import ApiService from "../../services/ApiService";

function* handleCategoriesRequestSaga() {
  try {
    const data = yield call(ApiService.getCategories);
    yield put(fetchSuccess(data));
  } catch (e) {
    yield put(fetchFailed(e.message));
  }
}

function* watchCategoriesSaga() {
  yield takeLatest(fetchRequest.type, handleCategoriesRequestSaga);
}

export default watchCategoriesSaga;
