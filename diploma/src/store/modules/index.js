import { combineReducers } from "@reduxjs/toolkit";

import bestseller from "./bestseller";
import detail from "./detail";
import catalog from "./catalog";
import categories from "./categories";
import cart from "./cart";
import notice from "./notice";

export default combineReducers({
  bestseller,
  detail,
  catalog,
  categories,
  cart,
  notice
});
