import { createSlice } from "@reduxjs/toolkit";
import fetchReducers from "./fetchReducers";

const OFFSET_SIZE = 6;
const INITIAL_STATE = {
  list: [],
  loading: false,
  error: null,
  offset: 0,
  categoryId: null,
  categories: [],
  allLoaded: false,
  search: "",
};

const fetchCatalogReducers = fetchReducers();

const slice = createSlice({
  name: "catalog",
  initialState: INITIAL_STATE,
  reducers: {
    ...fetchCatalogReducers,
    fetchSuccess: (state, action) => {
      if (action.payload.length < OFFSET_SIZE) {
        state.allLoaded = true;
      }
      state.list.push(...action.payload);
      state.loading = false;
    },
    addOffset: (state) => {
      state.offset = state.offset + OFFSET_SIZE;
    },
    clearCatalog: (state) => {
      state.offset = 0;
      state.list = [];
      state.allLoaded = false;
    },
    setSearch: (state, action) => {
      state.search = action.payload;
    },
  },
});

export const {
  fetchRequest,
  fetchSuccess,
  fetchFailed,
  addOffset,
  clearCatalog,
  setSearch,
} = slice.actions;

export default slice.reducer;
