import { createSlice } from "@reduxjs/toolkit";
import fetchReducers from "./fetchReducers";

const INITIAL_STATE = {
  list: [],
  loading: false,
  error: null,
};

const fetchBestseller = fetchReducers();

const slice = createSlice({
  name: "bestseller",
  initialState: INITIAL_STATE,
  reducers: {
    ...fetchBestseller,
  },
});

export const { fetchRequest, fetchSuccess, fetchFailed } = slice.actions;

export default slice.reducer;
