import { createSlice } from "@reduxjs/toolkit";
import fetchReducers from "./fetchReducers";

const INITIAL_STATE = {
  list: [],
  loading: false,
  error: null,
  activeCategoryId: null,
};

const categoriesReducers = fetchReducers();

const slice = createSlice({
  name: "categories",
  initialState: INITIAL_STATE,
  reducers: {
    ...categoriesReducers,
    setActiveCategoryId: (state, action) => {
      const { categoryId } = action.payload;
      state.activeCategoryId = categoryId;
    },
  },
});

export const {
  fetchRequest,
  fetchSuccess,
  fetchFailed,
  setActiveCategoryId,
} = slice.actions;

export default slice.reducer;
