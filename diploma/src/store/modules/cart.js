import { createSlice } from "@reduxjs/toolkit";

const INITIAL_STATE = {
  items: {},
  phone: "",
  address: "",
};

function getItemId(item) {
  const { id, size } = item;
  return `${id}_${size}`;
}

const slice = createSlice({
  name: "cart",
  initialState: INITIAL_STATE,
  reducers: {
    loadCart: (state, action) => {
      const cart = localStorage.getItem("cart");
      if (cart) {
        state.items = JSON.parse(cart);
      }
    },
    addItem: (state, action) => {
      const item_id = getItemId(action.payload);
      if (!state.items[item_id]) {
        state.items[item_id] = action.payload;
      } else {
        state.items[item_id].count++;
      }
      localStorage.setItem("cart", JSON.stringify(state.items));
    },
    delItem: (state, action) => {
      const item = state.items[action.payload.id];
      const item_id = getItemId(item);
      delete state.items[item_id];
      localStorage.setItem("cart", JSON.stringify(state.items));
    },
    clearCart: (state) => {
      state.items = {};
      localStorage.removeItem("cart");
    }
  },
});

export const { addItem, delItem, loadCart, clearCart } = slice.actions;

export default slice.reducer;
