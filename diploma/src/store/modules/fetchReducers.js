const fetchReducers = (payloadFieldName = "list") => {
  return {
    fetchRequest: (state) => {
      state.loading = true;
      state.error = null;
    },
    fetchSuccess: (state, action) => {
      state[payloadFieldName] = action.payload;
      state.loading = false;
    },
    fetchFailed: (state, action) => {
      state.error = action.payload;
      state.loading = false;
    },
  };
};

export default fetchReducers;
