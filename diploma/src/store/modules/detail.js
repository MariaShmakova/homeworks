import { createSlice } from "@reduxjs/toolkit";
import fetchReducers from "./fetchReducers";

const COUNT_MAX = 10;
const COUNT_MIN = 1;

const INITIAL_STATE = {
  item: null,
  loading: false,
  error: null,
  size: null,
  count: COUNT_MIN,
};

const fetchDetailReducers = fetchReducers("item");

const slice = createSlice({
  name: "detail",
  initialState: INITIAL_STATE,
  reducers: {
    ...fetchDetailReducers,
    addCount: (state) => {
      if (state.count < COUNT_MAX) state.count++;
    },
    subtractCount: (state) => {
      if (state.count > COUNT_MIN) state.count--;
    },
    selectSize: (state, action) => {
      state.size = state.size === action.payload ? null : action.payload;
    },
    resetDetailData: (state) => {
      Object.assign(state, INITIAL_STATE);
    },
  },
});

export const {
  fetchRequest,
  fetchSuccess,
  fetchFailed,
  addCount,
  subtractCount,
  selectSize,
  resetDetailData,
} = slice.actions;

export default slice.reducer;
