import { createSlice } from "@reduxjs/toolkit";
import { nanoid } from "nanoid";

const INITIAL_STATE = {
  list: [],
};

const NOTICE_TYPE = {
  ERROR: "ERROR",
  SUCCESS: "SUCCESS",
};

function Notice(message, type) {
  return {
    id: nanoid(),
    message,
    type,
    created_at: new Date().toJSON(),
  };
}

const slice = createSlice({
  name: "notice",
  initialState: INITIAL_STATE,
  reducers: {
    addNotice: (state, action) => {
      state.list.push(new Notice(action.payload, NOTICE_TYPE.SUCCESS));
    },
    addNoticeError: (state, action) => {
      state.list.push(new Notice(action.payload, NOTICE_TYPE.ERROR));
    },
    removeNotice: (state, action) => {
      state.list = state.list.filter((item) => item.id !== action.payload);
    },
  },
});

export const { addNotice, removeNotice, addNoticeError } = slice.actions;

export default slice.reducer;
