import React from "react";
import Catalog from "../components/Catalog";

function CatalogPage() {
  return <Catalog search />;
}

export default CatalogPage;
