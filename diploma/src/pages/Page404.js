import React from "react";

function Page404() {
  return (
    <section className="common-page">
      <h2 className="text-center">Страница не найдена</h2>
      <p>Извините, такая страница не найдена!</p>
    </section>
  );
}

export default Page404;
