import React from "react";
import Catalog from "../components/Catalog";
import TopSales from "../components/Catalog/TopSales";

function PageMain() {
  return (
    <>
      <TopSales />
      <Catalog />
    </>
  );
}

export default PageMain;
