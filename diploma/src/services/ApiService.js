import axios from "axios";
import getQueryURLByParams from "../utils/getURLParams";

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

const ApiService = {
  getTopSales: async () => {
    try {
      const response = await api.get("/top-sales");
      return response.data;
    } catch (error) {
      throw new Error(error.message);
    }
  },
  getCategories: async () => {
    try {
      const response = await api.get("/categories");
      return response.data;
    } catch (error) {
      throw new Error(error.message);
    }
  },
  getItems: async (params = {}) => {
    const query = getQueryURLByParams(params);

    try {
      const response = await api.get(`/items?${query}`);
      return response.data;
    } catch (error) {
      throw new Error(error.message);
    }
  },
  getItem: async (id) => {
    try {
      const response = await api.get(`/items/${id}`);
      return response.data;
    } catch (error) {
      throw new Error(error.message);
    }
  },
  makeOrder: async (data) => {
    try {
      const response = await api.post(`/order`, data);
      return response.status === 204;
    } catch (error) {
      throw new Error(error.message);
    }
  },
};

export default ApiService;
