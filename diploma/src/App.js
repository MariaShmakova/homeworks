import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PageMain from "./pages/PageMain";
import PageAbout from "./pages/PageAbout";
import Page404 from "./pages/Page404";
import CatalogPage from "./pages/CatalogPage";
import PageCart from "./pages/PageCart";
import PageContacts from "./pages/PageContacts";
import PageProduct from "./pages/PageProduct";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Banner from "./components/Banner";
import Notices from "./components/Notices";

function App() {
  return (
    <Router>
      <Header />
      <main className="container">
        <div className="row">
          <div className="col">
            <Banner />
            <Switch>
              <Route path="/catalog" component={CatalogPage} />
              <Route path="/about" component={PageAbout} />
              <Route path="/contacts" component={PageContacts} />
              <Route path="/cart" component={PageCart} />
              <Route path="/products/:id" component={PageProduct} />
              <Route path="/" exact component={PageMain} />
              <Route path="*" component={Page404} />
            </Switch>
          </div>
        </div>
        <Notices />
      </main>
      <Footer />
    </Router>
  );
}

export default App;
