import React from "react";
import "./Footer.css";
import FooterNav from "./FooterNav";
import FooterPay from "./FooterPay";
import FooterContacts from "./FooterContacts";
import FooterCopyright from "./FooterCopyright";

function Footer() {
  return (
    <footer className="container bg-light footer">
      <div className="row">
        <div className="col">
          <FooterNav />
        </div>
        <div className="col">
          <FooterPay />
          <FooterCopyright />
        </div>
        <div className="col text-right">
          <FooterContacts />
        </div>
      </div>
    </footer>
  );
}

export default Footer;
