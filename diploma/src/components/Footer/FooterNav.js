import React from "react";
import { Link } from "react-router-dom";

function FooterNav() {
  const navs = [
    {
      url: "/about",
      name: "О магазине",
    },
    {
      url: "/catalog",
      name: "Каталог",
    },
    {
      url: "/contacts",
      name: "Контакты",
    },
  ];
  return (
    <section>
      <h5>Информация</h5>
      <ul className="nav flex-column">
        {navs.map(({ url, name, props }) => (
          <li key={url} className="nav-item">
            <Link to={url} className="nav-link">
              {name}
            </Link>
          </li>
        ))}
      </ul>
    </section>
  );
}

export default FooterNav;
