import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchRequest, resetDetailData } from "../../store/modules/detail";
import Loader from "../Loader";
import { useRouteMatch } from "react-router-dom";
import "./CatalogItemDetail.css";
import Detail from "./Detail";
import Error from "../Error";

function CatalogItemDetail() {
  const { item, loading, error, size, count } = useSelector(
    (state) => state.detail
  );
  const dispatch = useDispatch();
  const match = useRouteMatch();
  const id = +match.params.id;

  function handleLoadItems() {
    dispatch(fetchRequest(id));
  }

  useEffect(() => {
    dispatch(resetDetailData());
    handleLoadItems();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <section className="catalog-item">
      {loading && <Loader />}
      {error && <Error handleRetry={handleLoadItems} />}
      {!loading && item && <Detail item={item} size={size} count={count} />}
    </section>
  );
}

export default CatalogItemDetail;
