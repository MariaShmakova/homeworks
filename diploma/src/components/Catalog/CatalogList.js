import React, { useEffect } from "react";
import CatalogItem from "./CatalogItem";
import { useDispatch, useSelector } from "react-redux";
import { clearCatalog, fetchRequest } from "../../store/modules/catalog";
import Loader from "../Loader";
import Error from "../Error";
import { addOffset } from "../../store/modules/catalog";
import useEffectOnlyOnUpdate from "../../hooks/useEffectOnlyOnUpdate";

function CatalogList() {
  const dispatch = useDispatch();
  const { list, loading, error, offset, allLoaded, search } = useSelector(
    (state) => state.catalog
  );

  const { activeCategoryId } = useSelector((state) => state.categories);

  const changeOffset = () => {
    dispatch(addOffset());
  };

  const handleLoadItems = () => {
    dispatch(fetchRequest({ offset, categoryId: activeCategoryId, q: search }));
  };

  useEffect(() => {
    !list || (!list.length && handleLoadItems());
  }, []);

  useEffectOnlyOnUpdate(() => {
    handleLoadItems();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offset, activeCategoryId, search]);

  useEffectOnlyOnUpdate(() => {
    dispatch(clearCatalog());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeCategoryId, search]);

  const EmptyResult = () => (
    <div>Поиск по строке "{search}" не дал результатов.</div>
  );

  return (
    <>
      {error && <Error handleRetry={handleLoadItems} />}
      {!!list.length && (
        <div className="row">
          {list.map((item) => (
            <div key={item.id} className="col-4">
              <CatalogItem {...item} />
            </div>
          ))}
        </div>
      )}
      {loading && (
        <div className="row mb-3">
          <Loader />
        </div>
      )}
      {!!list.length && !allLoaded && (
        <div className="text-center">
          <button
            className="btn btn-outline-primary"
            onClick={changeOffset}
            disabled={loading}
          >
            Загрузить ещё
          </button>
        </div>
      )}
      {!list.length && search && <EmptyResult />}
    </>
  );
}

export default CatalogList;
