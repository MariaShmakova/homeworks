import React from "react";

function SizeAvailable({ sizes, activeSize, handleSelectSize }) {
  return (
    <p>
      Размеры в наличии:
      {sizes
        .filter((item) => item.avalible)
        .map((item) => (
          <span
            key={item.size}
            className={`catalog-item-size ${
              activeSize === item.size ? "selected" : ""
            }`}
            onClick={() => handleSelectSize(item.size)}
          >
            {item.size}
          </span>
        ))}
    </p>
  );
}

export default SizeAvailable;
