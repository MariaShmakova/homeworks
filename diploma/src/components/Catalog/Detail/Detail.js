import React from "react";
import SizeAvailable from "./SizeAvailable";
import Count from "./Count";
import { useDispatch } from "react-redux";
import {
  addCount,
  selectSize,
  subtractCount,
} from "../../../store/modules/detail";
import { addItem } from "../../../store/modules/cart";
import { useHistory } from "react-router-dom";

function Detail({ item, size, count }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const handleAdd = () => {
    dispatch(addCount());
  };

  const handleSubtract = () => {
    dispatch(subtractCount());
  };
  const handleSelectSize = (newSize) => {
    dispatch(selectSize(newSize));
  };

  const handleAddCart = () => {
    dispatch(
      addItem({
        id: item.id,
        title: item.title,
        size: size,
        count: count,
        price: item.price,
      })
    );
    history.push("/cart");
  };

  const img = Array.isArray(item.images) ? item.images[0] : item.images;
  return (
    <>
      <h2 className="text-center">{item.title}</h2>
      <div className="row">
        <div className="col-5">
          <img src={img} className="img-fluid" alt="" />
        </div>
        <div className="col-7">
          <table className="table table-bordered">
            <tbody>
              <tr>
                <td>Артикул</td>
                <td>{item.sku}</td>
              </tr>
              <tr>
                <td>Производитель</td>
                <td>{item.manufacturer}</td>
              </tr>
              <tr>
                <td>Цвет</td>
                <td>{item.color}</td>
              </tr>
              <tr>
                <td>Материалы</td>
                <td>{item.material}</td>
              </tr>
              <tr>
                <td>Сезон</td>
                <td>{item.season}</td>
              </tr>
              <tr>
                <td>Повод</td>
                <td>{item.reason}</td>
              </tr>
            </tbody>
          </table>
          <div className="text-center">
            <SizeAvailable
              sizes={item.sizes}
              activeSize={size}
              handleSelectSize={handleSelectSize}
            />
            <Count
              count={count}
              handleAdd={handleAdd}
              handleSubtract={handleSubtract}
            />
          </div>
          <button
            className="btn btn-danger btn-block btn-lg"
            disabled={!size}
            onClick={handleAddCart}
          >
            В корзину
          </button>
        </div>
      </div>
    </>
  );
}

export default Detail;
