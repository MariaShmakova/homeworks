import React from "react";

function Count({ count, handleAdd, handleSubtract }) {
  return (
    <p>
      Количество:
      <span className="btn-group btn-group-sm pl-2">
        <button className="btn btn-secondary" onClick={handleSubtract}>
          -
        </button>
        <span className="btn btn-outline-primary">{count}</span>
        <button className="btn btn-secondary" onClick={handleAdd}>
          +
        </button>
      </span>
    </p>
  );
}

export default Count;
