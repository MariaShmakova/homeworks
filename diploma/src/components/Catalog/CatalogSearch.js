import React, { useEffect, useState } from "react";
import "./CatalogSearch.css";
import { useDispatch, useSelector } from "react-redux";
import { setSearch } from "../../store/modules/catalog";

function CatalogSearch() {
  const [value, setValue] = useState("");
  const { search } = useSelector((state) => state.catalog);
  const dispatch = useDispatch();

  useEffect(() => {
    setValue(search);
  }, [search]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(setSearch(value));
  };

  const handleInputSearch = (e) => {
    e.preventDefault();
    setValue(e.target.value);
  };

  return (
    <form className="catalog-search-form form-inline" onSubmit={handleSubmit}>
      <input
        className="form-control"
        placeholder="Поиск"
        value={value}
        onInput={handleInputSearch}
      />
    </form>
  );
}

export default CatalogSearch;
