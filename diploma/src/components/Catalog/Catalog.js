import React from "react";
import CatalogFilter from "./CatalogFilter";
import CatalogList from "./CatalogList";
import "./Catalog.css";
import CatalogSearch from "./CatalogSearch";

function Catalog({ search = false }) {
  return (
    <section className="catalog">
      <h2 className="text-center">Каталог</h2>
      {search && <CatalogSearch />}
      <CatalogFilter />
      <CatalogList />
    </section>
  );
}

export default Catalog;
