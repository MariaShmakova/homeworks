import React from "react";
import formatCurrency from "../../utils/formatCurrency";
import { Link } from "react-router-dom";
import "./CatalogItem.css";

function CatalogItem({ id, title, price, images }) {
  const img = Array.isArray(images) ? images[0] : images;
  return (
    <div className="card catalog-item-card">
      <div className="catalog-item-img">
        <img src={img} className="card-img-top img-fluid" alt={title} />
      </div>
      <div className="card-body">
        <p className="card-text text-truncate" title={title}>
          {title}
        </p>
        <p className="card-text">{formatCurrency(price)}</p>
        <Link to={`/products/${id}`} className="btn btn-outline-primary">
          Заказать
        </Link>
      </div>
    </div>
  );
}

export default CatalogItem;
