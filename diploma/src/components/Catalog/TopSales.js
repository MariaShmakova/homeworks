import React, { useEffect } from "react";
import CatalogItem from "./CatalogItem";
import { useDispatch, useSelector } from "react-redux";
import { fetchRequest } from "../../store/modules/bestseller";
import Loader from "../Loader";
import Error from "../Error";

function TopSales() {
  const { list, loading, error } = useSelector((state) => state.bestseller);
  const dispatch = useDispatch();

  useEffect(() => {
    handleLoadItems();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLoadItems = () => {
    dispatch(fetchRequest());
  };

  return (
    <section className="top-sales">
      <h2 className="text-center">Хиты продаж!</h2>
      {loading && <Loader />}
      {error && <Error handleRetry={handleLoadItems} />}
      {!loading && list && (
        <div className="row">
          {list.map((item) => (
            <div className="col-4" key={item.id}>
              <CatalogItem {...item} />
            </div>
          ))}
        </div>
      )}
    </section>
  );
}

export default TopSales;
