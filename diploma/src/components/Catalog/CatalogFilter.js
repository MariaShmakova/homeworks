import React, { useEffect } from "react";
import "./CatalogFilter.css";
import { useDispatch, useSelector } from "react-redux";
import classNames from "classnames";
import {
  fetchRequest,
  setActiveCategoryId,
} from "../../store/modules/categories";
import Error from "../Error";

function CatalogFilter(props) {
  const dispatch = useDispatch();
  const { list, activeCategoryId, loading, error } = useSelector(
    (state) => state.categories
  );

  const handleFetchRequest = () => {
    dispatch(fetchRequest());
  };

  useEffect(() => {
    !list.length && handleFetchRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setCategory = (e, categoryId) => {
    e.preventDefault();
    dispatch(setActiveCategoryId({ categoryId }));
  };

  const categories = [{ title: "Все", id: null }, ...list];

  if (loading) {
    return <></>;
  }

  if (error) {
    return <Error handleRetry={handleFetchRequest} />;
  }

  return (
    <ul className="catalog-categories nav justify-content-center">
      {categories.map((item) => (
        <li className="nav-item" key={item.id}>
          <a
            className={classNames("nav-link", {
              active: activeCategoryId === item.id,
            })}
            onClick={(e) => setCategory(e, item.id)}
            href={`#${item.id}`}
          >
            {item.title}
          </a>
        </li>
      ))}
    </ul>
  );
}

export default CatalogFilter;
