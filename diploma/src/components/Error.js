import React from "react";

function Error({ handleRetry }) {
  return (
    <div className="text-center">
      Произошла ошибка!
      {handleRetry && (
        <button className="btn btn-outline-primary ml-2" onClick={handleRetry}>
          Повторить запрос
        </button>
      )}
    </div>
  );
}

export default Error;
