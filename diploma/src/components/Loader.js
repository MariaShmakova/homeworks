import React from "react";
import "./Loader.css";
import classNames from "classnames";

function Loader({ small }) {
  return (
    <div className={classNames("preloader", { "-small": small })}>
      <span />
      <span />
      <span />
      <span />
    </div>
  );
}

export default Loader;
