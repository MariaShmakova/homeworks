import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ApiService from "../../services/ApiService";
import Loader from "../Loader";
import { clearCart } from "../../store/modules/cart";
import { addNotice, addNoticeError } from "../../store/modules/notice";

const INITIAL_FORM = {
  phone: "",
  address: "",
  agreement: false,
};

function Order() {
  const [form, setForm] = useState(INITIAL_FORM);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const { items } = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  const handleChange = (e) => {
    const value =
      e.target.type === "checkbox" ? e.target.checked : e.target.value;
    const name = e.target.name;

    setForm((prevState) => ({ ...prevState, [name]: value }));
  };

  const submit = async (e) => {
    e.preventDefault();
    const { phone, address } = form;
    const data = {
      owner: {
        phone,
        address,
      },
      items: Object.keys(items).map((item_id) => {
        const { id, price, count } = items[item_id];
        return {
          id,
          price,
          count,
        };
      }),
    };
    setLoading(true);
    setError(null);
    try {
      const res = await ApiService.makeOrder(data);
      setLoading(false);
      if (res) {
        dispatch(addNotice("Товар успешно заказан!"));
        dispatch(clearCart());
        setForm(INITIAL_FORM);
      }
    } catch (e) {
      dispatch(
        addNoticeError(
          "Произошла ошибка при оформлении заказа! Пожалуйста, повторите заказ позднее"
        )
      );
      setError(e.message);
    }
  };

  const isValid =
    form.phone && form.address && form.agreement && Object.keys(items).length;

  return (
    <section className="order">
      <h2 className="text-center">Оформить заказ</h2>
      <div className="card" style={{ maxWidth: "30rem", margin: "0 auto" }}>
        <form className="card-body" onSubmit={submit}>
          <div className="form-group">
            <label htmlFor="phone">Телефон</label>
            <input
              className="form-control"
              id="phone"
              name="phone"
              placeholder="Ваш телефон"
              value={form.phone}
              onChange={handleChange}
              disabled={loading}
            />
          </div>
          <div className="form-group">
            <label htmlFor="address">Адрес доставки</label>
            <input
              className="form-control"
              id="address"
              name="address"
              placeholder="Адрес доставки"
              value={form.address}
              onChange={handleChange}
              disabled={loading}
            />
          </div>
          <div className="form-group form-check">
            <input
              type="checkbox"
              className="form-check-input"
              id="agreement"
              name="agreement"
              checked={form.agreement}
              onChange={handleChange}
              disabled={loading}
            />
            <label className="form-check-label" htmlFor="agreement">
              Согласен с правилами доставки
            </label>
          </div>
          {error && "Произошла ошибка. Повторите запрос еще раз"}
          <button
            type="submit"
            className="btn btn-outline-secondary"
            disabled={!isValid || loading}
          >
            {loading && <Loader small />} Оформить
          </button>
        </form>
      </div>
    </section>
  );
}

export default Order;
