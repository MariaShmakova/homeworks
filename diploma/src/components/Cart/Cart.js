import React from "react";
import CartItems from "./CartItems";
import Order from "./Order";

function Cart() {
  return (
    <>
      <CartItems />
      <Order />
    </>
  );
}

export default Cart;
