import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { delItem } from "../../store/modules/cart";
import formatCurrency from "../../utils/formatCurrency";
import { Link } from "react-router-dom";

function CartItems() {
  const { items } = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  const deleteItem = (id) => {
    dispatch(delItem({ id }));
  };

  const totalCount = Object.keys(items).reduce(
    (sum, id) => sum + items[id].count * items[id].price,
    0
  );
  return (
    <section className="cart">
      <h2 className="text-center">Корзина</h2>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Размер</th>
            <th scope="col">Кол-во</th>
            <th scope="col">Стоимость</th>
            <th scope="col">Итого</th>
            <th scope="col">Действия</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(items).map((key, index) => {
            const { title, size, count, price, id } = items[key];
            return (
              <tr key={key}>
                <th scope="row">{index + 1}</th>
                <td>
                  <Link to={`/products/${id}`}>{title}</Link>
                </td>
                <td>{size}</td>
                <td>{count}</td>
                <td>{formatCurrency(price)}</td>
                <td>{formatCurrency(count * price)}</td>
                <td>
                  <button
                    className="btn btn-outline-danger btn-sm"
                    onClick={() => deleteItem(key)}
                  >
                    Удалить
                  </button>
                </td>
              </tr>
            );
          })}

          <tr>
            <td colSpan="5" className="text-right">
              Общая стоимость
            </td>
            <td>{formatCurrency(totalCount)}</td>
          </tr>
        </tbody>
      </table>
    </section>
  );
}

export default CartItems;
