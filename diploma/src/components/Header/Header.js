import React from "react";
import Navbar from "./Navbar";
import "./Header.css";

function Header(props) {
  return (
    <header className="container">
      <div className="row">
        <div className="col">
          <Navbar />
        </div>
      </div>
    </header>
  );
}

export default Header;
