import React from "react";
import NavbarBrand from "./NavbarBrand";
import Menu from "./Menu";
import Search from "./Search";
import Cart from "./Cart";

function Navbar(props) {
  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <NavbarBrand />
      <div className="collapase navbar-collapse" id="navbarMain">
        <Menu />
        <div>
          <div className="header-controls-pics">
            <Search />
            <Cart />
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
