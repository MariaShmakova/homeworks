import React from "react";
import { NavLink } from "react-router-dom";

function Menu() {
  const activeClassName = "active";
  const navs = [
    {
      url: "/",
      name: "Главная",
      props: { exact: true },
    },
    {
      url: "/catalog",
      name: "Каталог",
    },
    {
      url: "/about",
      name: "О магазине",
    },
    {
      url: "/contacts",
      name: "Контакты",
    },
  ];
  return (
    <ul className="navbar-nav mr-auto">
      {navs.map(({ url, name, props }) => (
        <li key={url} className="nav-item">
          <NavLink
            to={url}
            className="nav-link"
            activeClassName={activeClassName}
            {...props}
          >
            {name}
          </NavLink>
        </li>
      ))}
    </ul>
  );
}

export default Menu;
