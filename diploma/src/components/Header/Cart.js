import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { loadCart } from "../../store/modules/cart";

function Cart() {
  const { items } = useSelector((state) => state.cart);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadCart());
  }, []);

  const goToCart = () => {
    history.push("/cart");
  };

  const count = Object.keys(items).length;
  return (
    <div
      className="header-controls-pic header-controls-cart"
      onClick={goToCart}
    >
      {!!count && <div className="header-controls-cart-full">{count}</div>}
      <div className="header-controls-cart-menu" />
    </div>
  );
}

export default Cart;
