import React, { useState, useEffect, useRef } from "react";
import { useDispatch } from "react-redux";
import classNames from "classnames";
import { setSearch } from "../../store/modules/catalog";
import { useHistory } from "react-router-dom";

function Search() {
  const [visible, setVisible] = useState(false);
  const inputRef = useRef(null);
  const dispatch = useDispatch();
  const history = useHistory();

  const toggleVisible = () => {
    setVisible((prevState) => !prevState);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(setSearch(inputRef.current.value));
    setVisible(false);
    inputRef.current.value = "";
    history.push("/catalog");
  };

  useEffect(() => {
    if (visible) {
      inputRef.current.focus();
    }
  }, [visible]);

  return (
    <>
      <form
        onSubmit={handleSubmit}
        className={classNames("header-controls-search-form", "form-inline", {
          invisible: !visible,
        })}
      >
        <input className="form-control" placeholder="Поиск" ref={inputRef} />
      </form>
      <div
        onClick={toggleVisible}
        className="header-controls-pic header-controls-search"
      />
    </>
  );
}

export default Search;
