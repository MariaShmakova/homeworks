import React from "react";
import { Link } from "react-router-dom";

function NavbarBrand() {
  return (
    <Link className="navbar-brand" to="/">
      <img src="/img/header-logo.png" alt="Bosa Noga" />
    </Link>
  );
}

export default NavbarBrand;
