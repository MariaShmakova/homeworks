import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { removeNotice } from "../store/modules/notice";
import "./Notices.css";

function Notices() {
  const { list } = useSelector((state) => state.notice);
  const dispatch = useDispatch();

  const handleRemove = (id) => {
    dispatch(removeNotice(id));
  };

  if (!list || !list.length) {
    return <></>;
  }

  return (
    <div className="toast-container p-3">
      {list.map((item) => (
        <div
          key={item.id}
          className="toast show"
          role="alert"
          aria-live="assertive"
          aria-atomic="true"
        >
          <div className="toast-header">
            <strong className="mr-auto text-danger">
              {item.type === "error" ? "Ошибка" : "Уведомление"}
            </strong>
            <button
              type="button"
              className="close"
              onClick={() => handleRemove(item.id)}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="toast-body">{item.message}</div>
        </div>
      ))}
    </div>
  );
}

export default Notices;
