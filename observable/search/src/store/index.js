import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { combineEpics, createEpicMiddleware } from "redux-observable";
import skillsReducers from "../reducers/skills";
import { changeSearchEpic, searchSkillsEpic } from "../epics";

const reducer = combineReducers({
  skills: skillsReducers,
});

const epic = combineEpics(changeSearchEpic, searchSkillsEpic);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const epicMiddleware = createEpicMiddleware();

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(epicMiddleware))
);

epicMiddleware.run(epic);

export default store;
