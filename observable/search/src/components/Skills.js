import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Error from "./Error";
import SkillsList from "./SkillsList";
import { changeSearchField } from "../actions/actionCreators";

function Skills() {
  const { items, loading, error, search } = useSelector(
    (state) => state.skills
  );
  const dispatch = useDispatch();

  const handleSearch = (evt) => {
    const { value } = evt.target;
    dispatch(changeSearchField(value));
  };

  const hasQuery = search.trim() !== "";
  return (
    <>
      <div>
        <input type="search" value={search} onChange={handleSearch} />
      </div>
      {!hasQuery && <div>Type something to search...</div>}
      {hasQuery && loading && <div>searching...</div>}
      {error ? <Error /> : <SkillsList items={items} />}
    </>
  );
}

export default Skills;
