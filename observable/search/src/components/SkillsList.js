import React from "react";

function SkillsList({ items }) {
  return (
    <ul>
      {items.map((o) => (
        <li key={o.id}>{o.name}</li>
      ))}
    </ul>
  );
}

export default SkillsList;
