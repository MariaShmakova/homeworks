import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeFilter } from "../store/modules/services";
import Icon from "./Icon";

function ServiceFilter() {
  const filter = useSelector((state) => state.services.filter);
  const dispatch = useDispatch();

  const handleChange = (e) => {
    dispatch(changeFilter(e.target.value));
  };

  const handleClearFilter = () => {
    dispatch(changeFilter(""));
  };
  return (
    <div className="input-group mb-3">
      <input
        type="text"
        className="form-control"
        placeholder="Фильтр"
        aria-label="Фильтр"
        value={filter}
        onChange={handleChange}
      />
      <button className="btn btn-outline-secondary" onClick={handleClearFilter}>
        <Icon name="times" />
      </button>
    </div>
  );
}

export default ServiceFilter;
