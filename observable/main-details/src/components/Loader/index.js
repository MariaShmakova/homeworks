import Loader from "./Loader";
import LoaderXs from "./LoaderXS";

export { Loader, LoaderXs };

export default Loader;
