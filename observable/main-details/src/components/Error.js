import React from "react";

function Error({ text, handleRetry }) {
  return (
    <div className="alert alert-danger">
      {text}
      {handleRetry && (
        <button
          className="btn btn-outline-secondary ms-4"
          onClick={handleRetry}
        >
          Повторить запрос
        </button>
      )}
    </div>
  );
}

export default Error;
