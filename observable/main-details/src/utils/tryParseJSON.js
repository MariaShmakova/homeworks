export default function tryParseJSON(str) {
  try {
    const o = JSON.parse(str);
    if (o && typeof o === "object") {
      return o;
    }
  } catch (e) {}

  return false;
}
