import React from "react";
import ServiceFilter from "../components/ServiceFilter";
import ServiceList from "../components/ServiceList";

function Services() {
  return (
    <div className="row">
      <div className="col">
        <h2>Список услуг</h2>
        <ServiceFilter className="mb-3" />
        <ServiceList />
      </div>
    </div>
  );
}

export default Services;
