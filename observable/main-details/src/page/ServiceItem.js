import React from "react";
import ServiceForm from "../components/ServiceForm";

function ServiceItem() {
  return (
    <div className="row">
      <div className="col-md-6">
        <ServiceForm />
      </div>
    </div>
  );
}

export default ServiceItem;
