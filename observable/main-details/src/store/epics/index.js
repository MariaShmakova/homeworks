import { ofType } from "redux-observable";
import { ajax } from "rxjs/ajax";
import { map, switchMap, catchError } from "rxjs/operators";
import { of } from "rxjs";
import {
  fetchServicesRequest,
  fetchServicesSuccess,
  fetchServicesFailed,
} from "../modules/services";
import {
  fetchServiceDetailRequest,
  fetchServiceDetailSuccess,
  fetchServiceDetailFailed,
} from "../modules/serviceForm";
import routes from "../../utils/routes";

export const fetchServicesEpic = (action$) =>
  action$.pipe(
    ofType(fetchServicesRequest.type),
    switchMap(() =>
      ajax.getJSON(routes.getServices).pipe(
        map((o) => fetchServicesSuccess(o)),
        catchError((e) => of(fetchServicesFailed(e.message)))
      )
    )
  );

export const fetchServiceEpic = (action$) =>
  action$.pipe(
    ofType(fetchServiceDetailRequest.type),
    switchMap((o) =>
      ajax.getJSON(routes.getService(o.payload)).pipe(
        map((o) => fetchServiceDetailSuccess(o)),
        catchError((e) => of(fetchServiceDetailFailed(e.message)))
      )
    )
  );
