import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { combineEpics, createEpicMiddleware } from "redux-observable";
import reducer from "./modules";
import { fetchServicesEpic, fetchServiceEpic } from "./epics";

const epic = combineEpics(fetchServicesEpic, fetchServiceEpic);

const epicMiddleware = createEpicMiddleware();

const store = configureStore({
  reducer,
  middleware: [...getDefaultMiddleware(), epicMiddleware],
});

epicMiddleware.run(epic);

export default store;
