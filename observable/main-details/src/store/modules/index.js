import { combineReducers } from "redux";
import servicesReducer from "./services";
import serviceFormReducer from "./serviceForm";

export default combineReducers({
  services: servicesReducer,
  serviceForm: serviceFormReducer,
});
