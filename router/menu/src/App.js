import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import TimeAttackPage from "./components/Page/TimeAttackPage";
import DriftPage from "./components/Page/DriftPage";
import ForzaPage from "./components/Page/ForzaPage";
import HomePage from "./components/Page/HomePage";
import Menu from "./components/Menu";
import "./App.css";

function App() {
  return (
    <Router>
      <div>
        <Menu />
        <div className="page">
          <Route path="/" exact component={HomePage} />
          <Route path="/drift" component={DriftPage} />
          <Route path="/timeattack" component={TimeAttackPage} />
          <Route path="/forza" component={ForzaPage} />
        </div>
      </div>
    </Router>
  );
}

export default App;
