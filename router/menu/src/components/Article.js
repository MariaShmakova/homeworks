import React from "react";
import "./Article.css";

function Article({ title, paragraphs }) {
  return (
    <article className="article">
      <h1 className="article__title">{title}</h1>
      {paragraphs.map((item, key) => (
        <p key={`${title}${key}`} className="article__paragraph">
          {item}
        </p>
      ))}
    </article>
  );
}

export default Article;
