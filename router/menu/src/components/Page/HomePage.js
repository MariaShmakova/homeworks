import React from "react";
import Article from "../Article";

function HomePage() {
  const title = "Гоночное такси";
  const paragraphs = [
    `Гоночное такси – отличная возможность насладиться скоростью и мастерством гонщика,
        сидя на месте штурмана, и стать свидетелем настоящего мастерства профессиональных
        инструкторов Сочи Автодрома, в полной мере ощутив крутые виражи на самой современной
        гоночной трассе России.`,
  ];

  return <Article title={title} paragraphs={paragraphs} />;
}

export default HomePage;
