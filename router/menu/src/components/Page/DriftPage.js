import React from "react";
import Article from "../Article";

function DriftPage() {
  const title = "Дрифт-такси";
  const paragraphs = [
    `Только на Сочи Автодроме вас ждет уникальная возможность
     промчаться по трассе Формулы 1 на максимально возможной
     скорости в управляемом заносе на легендарной «королеве дрифта» Nissan Silvia!`,
    `Прокатитесь на дрифт-такси с вице-чемпионом Межконтинентального кубка по дрифту
     под эгидой FIA 2017 года Аркадием Цареградцевым и испытайте на себе, что значат
     скоростные постановки и эффектные заносы!`,
  ];

  return <Article title={title} paragraphs={paragraphs} />;
}

export default DriftPage;
