import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PostsList from "./pages/PostsList";
import PostNew from "./pages/PostNew";
import PostPage from "./pages/PostPage";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  return (
    <Router>
      <div className="p-4">
        <Switch>
          <Route path="/posts/new" component={PostNew} />
          <Route path="/posts/:id" component={PostPage} />
          <Route path="/" component={PostsList} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
