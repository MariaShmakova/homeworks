import { useState, useEffect } from "react";

export default function useApi(request, initialData) {
  const [data, setData] = useState(initialData);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    async function getData() {
      const data = await request();
      setData(data);
      setLoading(false);
    }
    getData();
  }, [request]);

  return { data, setData, isLoading };
}
