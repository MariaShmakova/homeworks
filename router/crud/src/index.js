import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import ApiService from "./services/ApiService";

window.api = new ApiService();

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
