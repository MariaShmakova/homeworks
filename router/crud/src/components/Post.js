import React from "react";
import moment from "moment";
import "moment/locale/ru";
import { Link } from "react-router-dom";
import PostAvatar from "./PostAvatar";
import PostInfo from "./PostInfo";

moment.locale("ru");

function Post({ id, created, content }) {
  return (
    <div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
      <div className="col p-4 d-flex flex-column position-static">
        <PostInfo id={id} content={content} created={created} />
        <Link to={`/posts/${id}`}>Подробнее...</Link>
      </div>
      <div className="col-auto d-none d-lg-block">
        <PostAvatar />
      </div>
    </div>
  );
}

export default Post;
