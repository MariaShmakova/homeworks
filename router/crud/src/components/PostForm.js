import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { LoaderXs } from "./Loader";

function PostForm({ content, id, onSave }) {
  const history = useHistory();
  const [text, setText] = useState(
    content || localStorage.getItem("text") || ""
  );
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const data = await window.api.addPost({ id, content: text });
    setLoading(false);

    if (data) {
      localStorage.removeItem("text");
      redirect();
    }
  };

  const redirect = () => {
    if (!id) {
      history.push("/");
    } else {
      onSave(text);
    }
  };

  const handleClose = () => {
    if (!id) {
      text && localStorage.setItem("text", text);
    }
    redirect();
  };

  const handleTextChange = (e) => {
    setText(e.target.value);
  };

  return (
    <form onSubmit={handleSubmit} className="">
      <button
        type="button"
        className="btn-close mb-2"
        aria-label="Close"
        onClick={handleClose}
      />
      <div className="mb-3">
        <div className="form-floating">
          <textarea
            className="form-control"
            placeholder="Напишите несколько строк"
            id="text"
            value={text}
            onChange={handleTextChange}
          />
          <label htmlFor="text">Пост</label>
        </div>
      </div>
      <button type="submit" className="btn btn-primary" disabled={loading}>
        {loading && <LoaderXs />}
        {id ? "Сохранить" : "Опубликовать"}
      </button>
    </form>
  );
}

export default PostForm;
