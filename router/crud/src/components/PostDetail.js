import React, { useState } from "react";
import moment from "moment";
import "moment/locale/ru";
import { useHistory } from "react-router-dom";
import { LoaderXs } from "./Loader";
import PostForm from "./PostForm";
import PostInfo from "./PostInfo";
import PostAvatar from "./PostAvatar";

moment.locale("ru");

function PostDetail({ id, created, content, onSave }) {
  const [isDeleting, setDeleting] = useState(false);
  const [isEdit, setEdit] = useState(false);
  const history = useHistory();

  const handleDelete = () => {
    setDeleting(true);
    window.api
      .delPost(id)
      .then((r) => {
        history.push("/");
      })
      .finally(() => {
        setDeleting(false);
      });
  };
  const handleEdit = () => {
    setEdit(true);
  };

  const handleSave = (text) => {
    setEdit(false);
    onSave(text);
  };

  if (isEdit) {
    return (
      <>
        <h2>Редактирование поста</h2>
        <PostForm id={id} content={content} onSave={handleSave} />
      </>
    );
  }
  return (
    <div className="row g-0 border rounded flex-md-row mb-4 shadow-sm h-md-250 position-relative">
      <div className="col-auto d-none d-lg-block">
        <PostAvatar />
      </div>
      <div className="col p-4 d-flex flex-column position-static">
        <PostInfo id={id} content={content} created={created} />
        <div>
          <div className="btn-group">
            <button className="btn btn-outline-primary" onClick={handleEdit}>
              Редактировать
            </button>
            <button
              className="btn btn-danger"
              onClick={handleDelete}
              disabled={isDeleting}
            >
              {isDeleting && <LoaderXs />} Удалить
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PostDetail;
