import React from "react";
import moment from "moment";

function PostInfo({ id, created, content }) {
  return (
    <>
      <h3 className="mb-0">Пост №{id}</h3>
      <div className="mb-1 text-muted">{moment(created).fromNow()}</div>
      <p className="card-text mb-4">{content}</p>
    </>
  );
}

export default PostInfo;
