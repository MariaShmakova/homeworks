import axios from "axios";

const CancelToken = axios.CancelToken;
let getPostsSource;

export default class ApiService {
  api = axios.create({
    baseURL: process.env.REACT_APP_URL,
  });

  addPost = async ({ id = 0, content }) => {
    try {
      const res = await this.api.post("/posts", { id, content });
      return res.status === 204;
    } catch (e) {
      console.error(e);
    }
  };

  delPost = async (id) => {
    try {
      const res = await this.api.delete(`/posts/${id}`);
      return res.status === 204;
    } catch (e) {
      console.error(e);
    }
  };

  getPosts = async () => {
    try {
      getPostsSource && getPostsSource.cancel();
      getPostsSource = CancelToken.source();

      const res = await this.api.get("/posts", {
        cancelToken: getPostsSource.token,
      });
      return res.data;
    } catch (e) {
      if (axios.isCancel(e)) {
        return false;
      } else {
        console.error(e);
      }
    }
  };
}
