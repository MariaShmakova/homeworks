import React from "react";
import { Link } from "react-router-dom";
import Post from "../components/Post";
import Loader from "../components/Loader";
import useApi from "../hooks/useApi";

function EmptyPosts() {
  return <p>Создайте первый пост</p>;
}
function PostsList() {
  const { data: posts, isLoading } = useApi(window.api.getPosts, []);

  const isEmpty = !isLoading && !posts.length;

  return (
    <div className="row mb-2">
      <div className="mb-2">
        <Link to="/posts/new" className="btn btn-primary">
          Создать
        </Link>
      </div>
      <h1>Посты</h1>
      {isLoading && <Loader />}
      {!!posts.length &&
        posts.map((item) => (
          <div className="col-md-6" key={item.id}>
            <Post {...item} />
          </div>
        ))}
      {isEmpty && <EmptyPosts />}
    </div>
  );
}

export default PostsList;
