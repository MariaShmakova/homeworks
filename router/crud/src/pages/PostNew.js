import React from "react";
import PostForm from "../components/PostForm";

function PostNew() {
  return (
    <div className="post-new col-6">
      <h1>Страница создания</h1>
      <PostForm />
    </div>
  );
}

export default PostNew;
