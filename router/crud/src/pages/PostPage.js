import React from "react";
import Loader from "../components/Loader";
import useApi from "../hooks/useApi";
import PostDetail from "../components/PostDetail";

function PostPage({ match }) {
  const { data: posts, isLoading, setData: setPosts } = useApi(
    window.api.getPosts,
    []
  );

  const id = +match.params.id;
  const post = posts.find((item) => item.id === id);

  const handleSave = (content) => {
    if (content === post.content) {
      return;
    }

    setPosts((prevState) => [
      ...prevState.filter((item) => item.id !== id),
      { id, content, created: new Date() },
    ]);
  };

  return (
    <div className="post-page">
      <h1>Детальная информация </h1>
      {isLoading && <Loader />}
      {!isLoading && <PostDetail {...post} onSave={handleSave} />}
    </div>
  );
}

export default PostPage;
