import React from "react";
import { useState } from "react";
import Workout from "./models/Workout";
import AddWorkout from "./components/AddWorkout/AddWorkout";
import WorkoutList from "./components/WorkoutList/WorkoutList";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const [workouts, setWorkouts] = useState([]);
  const [item, setItem] = useState();

  const handleSubmit = (data) => {
    const workoutInSameDay = workouts.find(
      (item) => data.id !== item.id && item.date === data.date
    );

    if (workoutInSameDay) {
      editWorkout({
        ...data,
        id: workoutInSameDay.id,
        distance: +workoutInSameDay.distance + +data.distance,
      });
      if (data.id) {
        handleRemove(data.id);
      }
    } else if (data.id) {
      editWorkout(data);
    } else {
      addWorkout(data);
    }

    setItem();
  };

  const addWorkout = (data) => {
    setWorkouts((prevState) =>
      [...prevState, new Workout(data)].sort(sortWorkoutCallback)
    );
  };

  const editWorkout = (data) => {
    setWorkouts((prevState) =>
      [
        ...prevState.map((item) =>
          item.id === data.id ? new Workout(data) : item
        ),
      ].sort(sortWorkoutCallback)
    );
  };

  const handleEdit = (data) => {
    setItem(data);
  };

  const handleRemove = (id) => {
    setWorkouts((prevState) => prevState.filter((item) => item.id !== id));
  };

  const sortWorkoutCallback = (a, b) => {
    if (a.timestamp > b.timestamp) return 1;
    if (a.timestamp < b.timestamp) return -1;
    return 0;
  };

  return (
    <div className="App container mt-4">
      <h1>Учет тренировок</h1>
      <div className="row">
        <div className="col-md-4 col-xs-12 mb-4">
          <AddWorkout item={item} onSubmit={handleSubmit} />
        </div>
        <div className="col-md-8 col-xs-12">
          <WorkoutList
            list={workouts}
            onRemove={handleRemove}
            onEdit={handleEdit}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
