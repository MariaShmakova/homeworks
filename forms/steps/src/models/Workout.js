import { nanoid } from "nanoid";
import moment from "moment";

class Workout {
  constructor({ date, distance, dateFormat = "DD.mm.YYYY" }) {
    this.id = nanoid();
    this._date = moment(date, dateFormat);
    this.dateFormat = dateFormat;
    this.distance = distance;
  }

  get timestamp() {
    return this._date.valueOf();
  }

  get date() {
    return this._date.format(this.dateFormat);
  }
}

export default Workout;
