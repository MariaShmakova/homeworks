import React from "react";

function WorkoutList({ list, onRemove, onEdit }) {
  return (
    <table className="WorkoutList table">
      <thead>
        <tr>
          <td>Дата (ДД.ММ.ГГ)</td>
          <td>Пройдено км</td>
          <td>Действия</td>
        </tr>
      </thead>
      <tbody>
        {list.map((item) => (
          <tr key={item.id}>
            <td>{item.date}</td>
            <td>{item.distance}</td>
            <td>
              <button
                className="btn btn-outline-primary me-2"
                onClick={() => onEdit(item)}
              >
                Edit
              </button>
              <button
                className="btn btn-outline-danger"
                onClick={() => onRemove(item.id)}
              >
                Remove
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default WorkoutList;
