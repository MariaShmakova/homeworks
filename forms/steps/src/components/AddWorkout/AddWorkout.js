import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Workout from "../../models/Workout";

const INITIAL_FORM = {
  id: null,
  date: "",
  distance: "",
};

AddWorkout.propTypes = {
  item: PropTypes.instanceOf(Workout),
  onSubmit: PropTypes.func.isRequired,
};

function AddWorkout({ item = INITIAL_FORM, onSubmit }) {
  const [form, setForm] = useState(item);

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(form);
    setForm(INITIAL_FORM);
  };

  const handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;

    setForm((prevState) => ({ ...prevState, [name]: value }));
  };

  useEffect(() => {
    const { id, date, distance } = item;
    setForm({ id, date, distance });
  }, [item]);

  return (
    <form className="AddWorkout" onSubmit={handleSubmit}>
      <div className="mb-3">
        <label htmlFor="date" className="form-label">
          Дата (ДД.ММ.ГГ)
        </label>
        <input
          type="text"
          name="date"
          pattern="\d{2}\.\d{2}\.\d{4}"
          className="form-control"
          value={form.date}
          onChange={handleChange}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="distance" className="form-label">
          Пройдено км
        </label>
        <input
          type="number"
          name="distance"
          className="form-control"
          value={form.distance}
          min="0"
          onChange={handleChange}
        />
      </div>
      <button className="btn btn-primary">Ok</button>
    </form>
  );
}

export default AddWorkout;
