import "./App.css";
import React, { useState } from "react";

const INITIAL_STATE = {
  hex: "",
  rgb: "",
  error: false,
};

function App() {
  const [state, setState] = useState(INITIAL_STATE);

  const handleHexChange = ({ target }) => {
    const value = target.value;

    if (value.length !== 7) {
      setState({ ...INITIAL_STATE, hex: value });
      return;
    }

    const rgb = convertHexToRGB(value);

    setState((prevState) => ({ ...prevState, hex: value, error: !rgb, rgb }));
  };

  const convertHexToRGB = (hex) => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    if (result) {
      return `rgb(
        ${parseInt(result[1], 16)},
        ${parseInt(result[2], 16)},
        ${parseInt(result[3], 16)}
      )`;
    }

    return "";
  };

  const { hex, rgb, error } = state;

  return (
    <div className="App" style={{ backgroundColor: rgb }}>
      <input type="text" value={hex} onChange={handleHexChange} />
      <div className="App-result">
        {rgb}
        {error && "Ошибка!"}
      </div>
    </div>
  );
}

export default App;
