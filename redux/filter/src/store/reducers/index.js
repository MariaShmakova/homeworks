import { combineReducers } from "redux";
import serviceReducer from "./services";

export default combineReducers({
  services: serviceReducer,
});
