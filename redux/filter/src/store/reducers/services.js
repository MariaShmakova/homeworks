import { nanoid } from "nanoid";
import ACTIONS from "../actionTypes";

const {
  ADD_SERVICE,
  REMOVE_SERVICE,
  EDIT_SERVICE,
  UPDATE_SERVICE,
  CHANGE_FILTER,
} = ACTIONS.SERVICES;

const INITIAL_STATE = {
  list: [],
  edit_service_id: null,
  filter: "",
};

export default function serviceReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ADD_SERVICE: {
      const { name, price } = action.payload;
      return { ...state, list: [...state.list, { id: nanoid(), name, price }] };
    }
    case REMOVE_SERVICE:
      return {
        ...state,
        list: state.list.filter((item) => item.id !== action.payload.id),
      };
    case EDIT_SERVICE:
      return { ...state, edit_service_id: action.payload.id };
    case UPDATE_SERVICE: {
      const { id, name, price } = action.payload;
      return {
        ...state,
        edit_service_id: null,
        list: state.list.map((item) =>
          item.id === id ? { id, name, price } : item
        ),
      };
    }
    case CHANGE_FILTER:
      return { ...state, filter: action.payload.filter };
    default:
      return state;
  }
}
