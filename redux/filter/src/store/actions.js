import ACTIONS from "./actionTypes";

export const addService = ({ name, price }) => {
  return { type: ACTIONS.SERVICES.ADD_SERVICE, payload: { name, price } };
};

export const removeService = (id) => {
  return { type: ACTIONS.SERVICES.REMOVE_SERVICE, payload: { id } };
};

export const editServiceById = (id) => {
  return { type: ACTIONS.SERVICES.EDIT_SERVICE, payload: { id } };
};

export const updateService = ({ id, name, price }) => {
  return {
    type: ACTIONS.SERVICES.UPDATE_SERVICE,
    payload: { id, name, price },
  };
};

export const changeFilter = (filter) => {
  return {
    type: ACTIONS.SERVICES.CHANGE_FILTER,
    payload: { filter },
  };
};
