import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { editServiceById, removeService } from "../store/actions";
import Icon from "./Icon";

/**
 * Список сервисов
 *
 * @returns {JSX.Element}
 * @constructor
 */
function ServiceList() {
  const dispatch = useDispatch();
  const list = useSelector((state) => state.services.list);
  const filter = useSelector((state) => state.services.filter);
  const [filteredList, setFilteredList] = useState([]);

  useEffect(() => {
    if (!filter || !filter.length) {
      return setFilteredList(list);
    }

    setFilteredList(
      list.filter(
        (item) => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
      )
    );
  }, [filter, list]);

  const handleRemove = (id) => {
    dispatch(removeService(id));
  };

  const handleEdit = (id) => {
    dispatch(editServiceById(id));
  };

  const EmptyList = () => {
    if (filter && filter.length) {
      return <span>Под фильтр "{filter}" не подходит ни одна услуга!</span>;
    }
    return <span>Список услуг пуст!</span>;
  };

  return (
    <div className="service-list">
      <ul className="list-group">
        {filteredList.map((item) => (
          <li key={item.id} className="list-group-item">
            {item.name} - {item.price}
            <span className="btn-group float-end">
              <button
                className="btn btn-success"
                onClick={() => handleEdit(item.id)}
                title="Редактировать"
              >
                <Icon name="edit" />
              </button>
              <button
                className="btn btn-outline-danger"
                onClick={() => handleRemove(item.id)}
                title="Удалить"
              >
                <Icon name="trash" />
              </button>
            </span>
          </li>
        ))}
      </ul>
      {!filteredList.length && <EmptyList />}
    </div>
  );
}

export default ServiceList;
