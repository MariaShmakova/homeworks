import "./App.css";
import ServiceAdd from "./components/ServiceAdd";
import ServiceList from "./components/ServiceList";

function App() {
  return (
    <div className="container p-4">
      <div className="row">
        <div className="col">
          <ServiceAdd />
        </div>
        <div className="col">
          <ServiceList />
        </div>
      </div>
    </div>
  );
}

export default App;
