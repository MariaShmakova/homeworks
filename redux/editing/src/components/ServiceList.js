import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { editServiceById, removeService } from "../store/actions";
import Icon from "./Icon";

/**
 * Список сервисов
 *
 * @returns {JSX.Element}
 * @constructor
 */
function ServiceList() {
  const list = useSelector((state) => state.services.list);
  const dispatch = useDispatch();

  const handleRemove = (id) => {
    dispatch(removeService(id));
  };

  const handleEdit = (id) => {
    dispatch(editServiceById(id));
  };

  return (
    <ul className="list-group">
      {list.map((item) => (
        <li key={item.id} className="list-group-item">
          {item.name} - {item.price}
          <span className="btn-group float-end">
            <button
              className="btn btn-success"
              onClick={() => handleEdit(item.id)}
              title="Редактировать"
            >
              <Icon name="edit" />
            </button>
            <button
              className="btn btn-outline-danger"
              onClick={() => handleRemove(item.id)}
              title="Удалить"
            >
              <Icon name="trash" />
            </button>
          </span>
        </li>
      ))}
    </ul>
  );
}

export default ServiceList;
