import React, { useEffect, useState } from "react";
import { addService, editServiceById, updateService } from "../store/actions";
import { useDispatch, useSelector } from "react-redux";

const INITIAL_STATE = {
  name: "",
  price: "",
};

/**
 * Форма для добавления/редактирования сервиса
 *
 * @returns {JSX.Element}
 * @constructor
 */
function ServiceAdd() {
  const edit_service = useSelector((state) => {
    const { list, edit_service_id } = state.services;
    return list.find((item) => item.id === edit_service_id);
  });
  const [form, setForm] = useState(edit_service || INITIAL_STATE);
  const dispatch = useDispatch();

  useEffect(() => {
    setForm(edit_service || INITIAL_STATE);
  }, [edit_service]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (form.id) {
      dispatch(updateService(form));
    } else {
      dispatch(addService(form));
    }

    setForm(INITIAL_STATE);
  };

  const handleChange = (e) => {
    const { value, name } = e.target;
    setForm((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleCancel = (e) => {
    e.preventDefault();
    dispatch(editServiceById(null));
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-3">
        <label htmlFor="name">Название услуги</label>
        <input
          className="form-control"
          type="text"
          id="name"
          name="name"
          required
          value={form.name}
          onChange={handleChange}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="price">Цена</label>
        <input
          className="form-control"
          type="number"
          id="price"
          name="price"
          required
          value={form.price}
          onChange={handleChange}
        />
      </div>
      <div className="btn-group">
        <button type="submit" className="btn btn-primary">
          Сохранить
        </button>
        {form.id && (
          <button onClick={handleCancel} className="btn btn-outline-primary">
            Отмена
          </button>
        )}
      </div>
    </form>
  );
}

export default ServiceAdd;
