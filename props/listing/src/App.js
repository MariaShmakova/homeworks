import React from "react";
import "./App.css";
import items from "./data/items";
import Listing from "./components/Listing";

function App() {
  return (
    <div className="App">
      <Listing items={items} />
    </div>
  );
}

export default App;
