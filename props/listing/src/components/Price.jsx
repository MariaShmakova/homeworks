import React from "react";
import PropTypes from "prop-types";

Price.propTypes = {
  currency_code: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
};

function Price({ currency_code, price }) {
  if (currency_code === "USD") {
    return <p className="item-price">${price}</p>;
  }

  if (currency_code === "EUR") {
    return <p className="item-price">€{price}</p>;
  }

  return (
    <p className="item-price">
      {price} {currency_code}
    </p>
  );
}

export default Price;
