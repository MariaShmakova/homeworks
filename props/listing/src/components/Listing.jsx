import React from "react";
import PropTypes from "prop-types";
import ItemModel from "../models/ItemModel";
import Item from "./Item";

Listing.propTypes = {
  items: PropTypes.arrayOf(PropTypes.instanceOf(ItemModel)),
};

Listing.defaultProps = {
  items: [],
};

function Listing({ items }) {
  return (
    <div className="item-list">
      {items.map((item) => {
        const { listing_id, ...props } = item;
        return <Item {...props} key={listing_id} />;
      })}
    </div>
  );
}

export default Listing;
