import React from "react";
import PropTypes from "prop-types";

Quantity.propTypes = {
  count: PropTypes.number,
};

Quantity.defaultProps = {
  count: 0,
};

function Quantity({ count }) {
  let className = "item-quantity";
  let level = "";

  switch (true) {
    case count <= 10:
      level = "level-low";
      break;
    case count <= 20:
      level = "level-medium";
      break;
    case count > 20:
      level = "level-high";
      break;
    default:
      level = "";
  }
  className += ` ${level}`;
  return <p className={className}>{count} left</p>;
}

export default Quantity;
