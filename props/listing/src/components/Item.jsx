import React from "react";
import PropTypes from "prop-types";
import ItemModel from "../models/ItemModel";
import Price from "./Price";
import Quantity from "./Quantity";

ItemModel.propTypes = {
  item: PropTypes.instanceOf(ItemModel),
};

function Item({ url, MainImage, title, currency_code, price, quantity }) {
  const formatted_title =
    title.length > 50 ? title.substring(0, 50) + "..." : title;
  return (
    <div className="item">
      <div className="item-image">
        <a href={url}>
          <img src={MainImage.url_570xN} alt={title} />
        </a>
      </div>
      <div className="item-details">
        <p className="item-title">{formatted_title}</p>
        <Price currency_code={currency_code} price={price} />
        <Quantity count={quantity} />
      </div>
    </div>
  );
}

export default Item;
