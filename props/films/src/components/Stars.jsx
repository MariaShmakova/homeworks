import React from "react";
import PropTypes from "prop-types";
import Star from "./Star";

Stars.propTypes = {
  count: PropTypes.number,
};

Stars.defaultProps = {
  count: 0,
};

const MIN_COUNT = 1;
const MAX_COUNT = 5;

function Stars({ count }) {
  if (Number.isNaN(count) || count < MIN_COUNT || count > MAX_COUNT) {
    return null;
  }

  let stars = [];

  for (let i = MIN_COUNT; i <= MAX_COUNT; i++) {
    stars.push(i);
  }

  return (
    <ul className="card-body-stars">
      {stars.map((starId) => (
        <li key={starId}>
          <Star isActive={starId <= count} />
        </li>
      ))}
    </ul>
  );
}

export default Stars;
