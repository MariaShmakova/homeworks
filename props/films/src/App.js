import React from "react";
import "./App.css";
import Stars from "./components/Stars";

function App() {
  return (
    <div className="App">
      <Stars />
      <Stars count={3} />
      <Stars count={2} />
      <Stars count={1} />
    </div>
  );
}

export default App;
