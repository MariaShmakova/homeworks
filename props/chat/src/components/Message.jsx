import React from "react";
import PropTypes from "prop-types";

Message.propTypes = {
  from: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
  text: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
};

function Message({ from, text, time }) {
  return (
    <li>
      <div className="message-data">
        <span className="message-data-name">
          <i className="fa fa-circle online" /> {from.name}
        </span>
        <span className="message-data-time">{time}</span>
      </div>
      <div className="message my-message">{text}</div>
    </li>
  );
}

export default Message;
