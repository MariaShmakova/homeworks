import React from "react";
import PropTypes from "prop-types";
import Response from "./Response";
import Typing from "./Typing";
import Message from "./Message";

MessageHistory.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.oneOf(["message", "response", "typing"]),
      from: PropTypes.shape({
        name: PropTypes.string.isRequired,
      }).isRequired,
      text: PropTypes.string,
      time: PropTypes.string.isRequired,
    })
  ),
};

MessageHistory.defaultProps = {
  list: [],
};

function MessageHistory({ list }) {
  if (!list.length) {
    return null;
  }
  const renderMessage = ({ type, id, ...props }) => {
    switch (type) {
      case "message":
        return <Message key={id} {...props} />;
      case "response":
        return <Response key={id} {...props} />;
      default:
        return <Typing key={id} from={props.from} />;
    }
  };

  return (
    <ul className="MessageHistory">
      {list.map((item) => renderMessage(item))}
    </ul>
  );
}

export default MessageHistory;
