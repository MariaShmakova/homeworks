import React from "react";
import PropTypes from "prop-types";

Response.propTypes = {
  from: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
  text: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
};

function Response({ from, text, time }) {
  return (
    <li className="clearfix">
      <div className="message-data align-right">
        <span className="message-data-time">{time}</span> &nbsp; &nbsp;
        <span className="message-data-name">{from.name}</span>
        <i className="fa fa-circle me" />
      </div>
      <div className="message other-message float-right">{text}</div>
    </li>
  );
}

export default Response;
