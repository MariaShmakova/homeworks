import React from "react";
import PropTypes from "prop-types";

Typing.propTypes = {
  from: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
};

function Typing({ from }) {
  return <li>{from.name} typing...</li>;
}

export default Typing;
