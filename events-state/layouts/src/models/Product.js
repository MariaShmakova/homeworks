export default class Product {
  constructor({ name, color, price, img, id }) {
    this.name = name;
    this.color = color;
    this.price = price;
    this.img = img;
    this.id = id;
  }
}
