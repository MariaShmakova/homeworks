import React from "react";
import "./App.scss";
import Store from "./components/Store/Store";

function App() {
  return (
    <div className="App">
      <Store />
    </div>
  );
}

export default App;
