import React, { useState } from "react";
import IconSwitch from "../IconSwitch/IconSwitch";
import CardsView from "../CardsView/CardsView";
import ListView from "../ListView/ListView";
import products from "../../data/products";
import "./Store.scss";

const VIEW = {
  CARD: "card",
  LIST: "list",
};

function Store() {
  const [state, setState] = useState({ view: VIEW.CARD });

  const itemsView = (items) => {
    if (state.view === VIEW.CARD) {
      return <CardsView cards={items} />;
    }
    return <ListView items={items} />;
  };

  const icon = state.view === VIEW.CARD ? "view_list" : "view_module";

  const onSwitchView = () => {
    setState((prevState) => {
      const newView = prevState.view === VIEW.CARD ? VIEW.LIST : VIEW.CARD;
      return { ...prevState, view: newView };
    });
  };

  return (
    <div className="Store">
      <IconSwitch icon={icon} onSwitch={onSwitchView} />
      {itemsView(products)}
    </div>
  );
}

export default Store;
