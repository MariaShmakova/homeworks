import React from "react";
import PropTypes from "prop-types";
import ShopCard from "../ShopCard/ShopCard";
import Product from "../../models/Product";
import "./CardsView.scss";

CardsView.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.instanceOf(Product)),
};

function CardsView({ cards }) {
  return (
    <div className="CardsView">
      {cards.map((card) => {
        const { id, ...props } = card;
        return <ShopCard key={card.id} {...props} />;
      })}
    </div>
  );
}

export default CardsView;
