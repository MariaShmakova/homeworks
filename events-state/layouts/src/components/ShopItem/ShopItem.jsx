import React from "react";
import PropTypes from "prop-types";
import Product from "../../models/Product";
import "./ShopItem.scss";

ShopItem.propTypes = {
  item: PropTypes.instanceOf(Product),
};

function ShopItem({ name, color, price, img }) {
  return (
    <div className="ShopItem item">
      <div className="ShopItem-img">
        <img src={img} alt={name} />
      </div>
      <div className="ShopItem-title item-title">{name}</div>
      <div className="item-color">{color}</div>
      <div className="item-price">${price}</div>
      <button className="btn">Add to cart</button>
    </div>
  );
}

export default ShopItem;
