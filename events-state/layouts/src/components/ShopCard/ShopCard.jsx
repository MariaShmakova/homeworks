import React from "react";
import PropTypes from "prop-types";
import Product from "../../models/Product";
import "./ShopCard.scss";

ShopCard.propTypes = {
  card: PropTypes.instanceOf(Product),
};

function ShopCard({ name, price, color, img }) {
  console.log(name, price, color, img);
  return (
    <div className="ShopCard item">
      <div className="ShopCard-img">
        <img src={img} alt={name} />
      </div>
      <div className="ShopCard-header">
        <div className="item-title">{name}</div>
        <div className="item-color ">{color}</div>
      </div>
      <div className="ShopCard-footer">
        <div className="item-price">${price}</div>
        <button className="btn">Add to cart</button>
      </div>
    </div>
  );
}

export default ShopCard;
