import React from "react";
import PropTypes from "prop-types";
import "./IconSwitch.scss";

IconSwitch.propTypes = {
  icon: PropTypes.string.isRequired,
  onSwitch: PropTypes.func.isRequired,
};

function IconSwitch({ icon, onSwitch }) {
  return (
    <div className="IconSwitch">
      <button onClick={onSwitch}>
        <span className="material-icons">{icon}</span>
      </button>
    </div>
  );
}

export default IconSwitch;
