import React from "react";
import PropTypes from "prop-types";
import ShopItem from "../ShopItem/ShopItem";
import Product from "../../models/Product";

ListView.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.instanceOf(Product)),
};

function ListView({ items }) {
  return (
    <div className="ListView">
      {items.map((item) => {
        const { id, ...props } = item;
        return <ShopItem key={item.id} {...props} />;
      })}
    </div>
  );
}

export default ListView;
