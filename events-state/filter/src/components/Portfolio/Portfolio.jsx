import React, { useState } from "react";
import dataProjects from "../../data/projects";
import dataFilters from "../../data/filters";
import ProjectList from "../ProjectList/ProjectList";
import Toolbar from "../Toolbar/Toolbar";
import Project from "../../model/Project";
import "./Portfolio.css";

const FILTERED_ALL = "All";
const filters = [FILTERED_ALL, ...dataFilters];
const projects = dataProjects.map((item, id) => new Project({ ...item, id }));

function Portfolio() {
  const [selected, setSelected] = useState(
    filters && filters.length ? filters[0] : ""
  );

  const onSelectedFilter = (selected) => {
    setSelected(selected);
  };

  const filteredProjects = projects.filter(
    (item) => selected === FILTERED_ALL || item.category === selected
  );

  return (
    <div className="Portfolio">
      <Toolbar
        filters={filters}
        onSelectedFilter={onSelectedFilter}
        selected={selected}
      />
      <ProjectList projects={filteredProjects} />
    </div>
  );
}

export default Portfolio;
