import React from "react";
import PropTypes from "prop-types";
import "./Toolbar.css";

Toolbar.propTypes = {
  filters: PropTypes.array.isRequired,
  selected: PropTypes.string.isRequired,
  onSelectedFilter: PropTypes.func.isRequired,
};

function Toolbar({ filters, selected, onSelectedFilter }) {
  return (
    <ul className="Toolbar">
      {filters.map((filter) => (
        <li key={filter} className="Toolbar-item">
          <button
            onClick={() => onSelectedFilter(filter)}
            className={filter === selected ? "-selected" : ""}
          >
            {filter}
          </button>
        </li>
      ))}
    </ul>
  );
}

export default Toolbar;
