import React from "react";
import PropTypes from "prop-types";
import Project from "../../model/Project";
import "./ProjectList.css";

ProjectList.propTypes = {
  projects: PropTypes.arrayOf(PropTypes.instanceOf(Project)),
};

function ProjectList({ projects }) {
  return (
    <div className="ProjectsList">
      {projects.map((item) => (
        <div className="ProjectItem" key={item.id}>
          <img src={item.img} alt={`project img ${item.id}`} />
        </div>
      ))}
    </div>
  );
}

export default ProjectList;
