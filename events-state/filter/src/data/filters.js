import projects from "./projects";

const filters = [...new Set(projects.map((item) => item.category))];

export default filters;
