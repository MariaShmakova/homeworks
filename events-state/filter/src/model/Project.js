class Project {
  constructor({ id, img, category }) {
    this.id = id;
    this.img = img;
    this.category = category;
  }
}

export default Project;
