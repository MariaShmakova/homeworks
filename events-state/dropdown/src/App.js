import "./App.css";
import React from "react";
import Dropdown from "./components/Dropdown";

function App() {
  const items = [
    "Profile Information",
    "Change Password",
    "Become PRO",
    "Help",
    "Log Out",
  ];

  const buttonContent = (
    <>
      <span>Account Settings</span>
      <i className="material-icons">public</i>
    </>
  );

  return (
    <div className="App container">
      <Dropdown items={items} buttonContent={buttonContent} />
    </div>
  );
}

export default App;
