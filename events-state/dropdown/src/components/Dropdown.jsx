import React, { useState } from "react";
import DropdownList from "./DropdownList";
import PropTypes from "prop-types";

Dropdown.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  buttonContent: PropTypes.object.isRequired,
};

function Dropdown({ items, buttonContent }) {
  const [dropdownStatus, setDropdownStatus] = useState(false);
  const toggleDropdown = () => {
    setDropdownStatus((prevState) => !prevState);
  };
  let className = "Dropdown dropdown-wrapper";

  if (dropdownStatus) {
    className += " open";
  }

  return (
    <div className={className}>
      <button onClick={toggleDropdown} className="btn">
        {buttonContent}
      </button>
      <DropdownList items={items} />
    </div>
  );
}

export default Dropdown;
