import React, { useState } from "react";
import PropTypes from "prop-types";
import DropdownItem from "./DropdownItem";

DropdownList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
};

function DropdownList({ items }) {
  const [selected, setSelected] = useState(null);
  const handleClick = (item) => {
    setSelected(item);
  };
  return (
    <ul className="dropdown">
      {items.map((item) => (
        <DropdownItem
          key={item}
          isActive={item === selected}
          item={item}
          handleClick={() => handleClick(item)}
        />
      ))}
    </ul>
  );
}

export default DropdownList;
