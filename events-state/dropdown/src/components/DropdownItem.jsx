import React from "react";
import PropTypes from "prop-types";

DropdownItem.propTypes = {
  item: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
  handleClick: PropTypes.func.isRequired,
};

DropdownItem.defaultProps = {
  isActive: false,
};

function DropdownItem({ isActive, handleClick, item }) {
  return (
    <li className={`${isActive && "active"}`}>
      <a href={`#${item}`} onClick={handleClick}>
        {item}
      </a>
    </li>
  );
}

export default DropdownItem;
