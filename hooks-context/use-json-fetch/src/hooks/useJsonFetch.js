import { useState, useEffect } from "react";

export default function useJsonFetch(url, opts) {
  const [data, setData] = useState(opts);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    fetch(`${process.env.REACT_APP_URL}${url}`)
      .then((r) => {
        if (r.ok) {
          return r.json();
        }
        setError(r.statusText);
        return false;
      })
      .then((r) => {
        r && setData(r);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [url]);

  return [data, loading, error];
}
