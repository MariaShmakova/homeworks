import React from "react";
import Test from "./components/Test";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  return (
    <div className="App container mt-3">
      <div className="row">
        <div className="col">
          <Test url="/data" />
        </div>
        <div className="col">
          <Test url="/error" />
        </div>
        <div className="col">
          <Test url="/loading" />
        </div>
      </div>
    </div>
  );
}

export default App;
