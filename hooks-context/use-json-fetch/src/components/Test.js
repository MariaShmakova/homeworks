import React from "react";
import useJsonFetch from "../hooks/useJsonFetch";
import Loader from "./Loader";
import Error from "./Error";

function Test({ url }) {
  const [data, loading, error] = useJsonFetch(url);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <Error text={error} />;
  }

  return (
    <div className="Test card">
      <div className="card-body">{data && data.status}</div>
    </div>
  );
}

export default Test;
