import React from "react";

function Error({ text }) {
  return <div className="Error alert alert-danger">Ошибка: {text}</div>;
}

export default Error;
