import { useState, useEffect, useRef } from "react";
import axios from "axios";

const CancelToken = axios.CancelToken;

export default function useApi(url, initialData) {
  const [data, setData] = useState(initialData);
  const [isLoading, setLoading] = useState(false);
  let tokenSource = useRef(null);

  useEffect(() => {
    tokenSource.current && tokenSource.current.cancel();
    tokenSource.current = CancelToken.source();

    setLoading(true);
    axios
      .get(`${process.env.REACT_APP_URL}/${url}`, {
        cancelToken: tokenSource.current.token,
      })
      .then((r) => {
        setData(r.data);
      })
      .catch((e) => {
        if (!axios.isCancel(e)) {
          console.error(e);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, [url]);

  return [{ data, isLoading }];
}
