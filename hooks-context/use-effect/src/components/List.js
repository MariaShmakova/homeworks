import React from "react";

function List({ list, activeId, onSelect }) {
  return (
    <div className="List list-group">
      {list.map(({ id, name }) => {
        let className = "list-group-item";
        if (id === activeId) {
          className += " active";
        }
        return (
          <button key={id} className={className} onClick={() => onSelect(id)}>
            {name}
          </button>
        );
      })}
    </div>
  );
}

export default List;
