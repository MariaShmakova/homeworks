import React from "react";

import Loader from "./Loader";
import useApi from "../hooks/useApi";

function Details({ info }) {
  const { id, name } = info;
  const [{ data: details, isLoading }] = useApi(`/${id}.json`, {});

  return (
    <div className="Details card" style={{ width: "18rem" }}>
      {!isLoading && (
        <img src={details.avatar} className="card-img-top" alt={name} />
      )}
      <div className="card-body">
        <h5 className="card-title">
          {isLoading && <Loader />}
          {name}
        </h5>
      </div>
      <ul className="list-group list-group-flush">
        {!isLoading &&
          details.details &&
          Object.keys(details.details).map((item) => (
            <li className="list-group-item" key={item}>
              {item}: {details.details[item]}
            </li>
          ))}
      </ul>
    </div>
  );
}

export default Details;
