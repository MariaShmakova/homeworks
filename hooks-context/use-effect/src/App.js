import React, { useState } from "react";
import Details from "./components/Details";
import List from "./components/List";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Loader from "./components/Loader";
import useApi from "./hooks/useApi";

function App() {
  const [{ data: list, isLoading }] = useApi("/users.json", [], []);
  const [activeItem, setActiveItem] = useState(null);

  const handleSelect = (id) => {
    setActiveItem(list.find((item) => item.id === id));
  };

  return (
    <div className="App container">
      <div className="row">
        <div className="col">
          {isLoading && <Loader />}
          <List list={list} onSelect={handleSelect} activeId={activeItem?.id} />
        </div>
        <div className="col">{activeItem && <Details info={activeItem} />}</div>
      </div>
    </div>
  );
}

export default App;
